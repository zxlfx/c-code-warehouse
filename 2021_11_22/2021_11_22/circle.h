#pragma once
#include"point.h"
class circle
{
public:
	void set_r(int r);

	int ger_r();

	void set_center(point& center);

	point& get_center();

private:
	int m_r;
	point m_center;
};
