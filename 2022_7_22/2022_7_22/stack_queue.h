//#pragma once
//#include<deque>
//#include<vector>
//#include<list>
//#include<iostream>
//#include<cassert>
//using namespace std;
//
//namespace ds
//{
//	template<class T, class Con = deque<T>>
//	class stack
//	{
//	public:
//		void push(const T& x)
//		{
//			_c.push_back(x);
//		}
//		void pop()
//		{
//			assert(!empty());
//			_c.pop_back();
//		}
//
//		bool empty()
//		{
//			return _c.empty();
//		}
//
//		size_t size()
//		{
//			return _c.size();
//		}
//
//		const T& top()
//		{
//			return _c.back();
//		}
//
//	private:
//		Con _c;
//	};
//
//	template<class T, class Con = deque<T>>
//	class queue
//	{
//	public:
//		void push(const T& x)
//		{
//			_c.push_back(x);
//		}
//
//		void pop()
//		{
//			_c.pop_front();
//		}
//
//		size_t size()
//		{
//			return _c.size();
//		}
//
//		bool empty()
//		{
//			return _c.empty();
//		}
//
//		const T& front()
//		{
//			return _c.front();
//		}
//
//		const T& back()
//		{
//			return _c.back();
//		}
//	private:
//		Con _c;
//	};
//}
