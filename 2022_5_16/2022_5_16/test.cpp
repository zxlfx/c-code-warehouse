#include<iostream>
using namespace std;

//class A
//{
//public:
//	A()
//	{
//		cout << "A(a)" << endl;
//	}
//};
//class Stack
//{
//public:
//	Stack(int capacity = 4)
//	{
//		if (capacity == 0)
//		{
//			_a = nullptr;
//			_capacity = _size = 0;
//		}
//		else
//		{
//			_a = (int*)malloc(sizeof(int) * capacity);
//			_size = 0;
//			_capacity = capacity;
//		}
//	}
//	~Stack()
//	{
//		free(_a);
//		_a = nullptr;
//		_size = _capacity = 0;
//	}
//private:
//	int* _a;
//	int _capacity;
//	int _size;
//};
class Date
{
public:
	void Print()const
	{
		cout << _year << "年" << _month << "月" << _day << "日" << endl;
	}

	int GetMonthDay(int year, int month)
	{
		int m_arr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
		int day = m_arr[month];
		if (month == 2 && (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
		{
			day++;
		}
		return day;
	}

	Date(int year = 1, int month = 2, int day = 3)
	{
		_year = year;
		_month = month;
		_day = day;
		if (year < 0 || month <= 0 || month>12 || day <= 0 || day > GetMonthDay(year,month))
		{
			cout << "无效日期" << endl;
			cout << _year << "年" << _month << "月" << _day << "日" << endl;
		}
	}

	Date(const Date& d)//浅拷贝
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

	Date& operator+= (int day)
	{
		if (day < 0)
		{
			*this -= -day;
		}
		else
		{
			_day += day;
			while (_day > GetMonthDay(_year, _month))
			{
				_day -= GetMonthDay(_year, _month);
				_month++;
				if (_month > 12)
				{
					_year++;
					_month = 1;
				}
			}
			return *this;
		}
	}

	Date& operator-=(int day)
	{
		if (day < 0)
		{
			*this += -day;
		}
		else
		{
			_day -= day;
			while (_day <= 0)
			{
				_month--;
				if (_month == 0)
				{
					_year--;
					_month = 12;
				}
				_day += GetMonthDay(_year, _month);
			}
		}
		return *this;
	}

	Date operator-(int day)const
	{
		Date temp = *this;
		temp -= day;
		return temp;
	}

	Date& operator=(const Date& d)
	{
		if (this != &d)
		{
			_year = d._year;
			_month = d._month;
			_day = d._day;
		}
		return *this;
	}

	int operator-(const Date& d)const
	{
		int flag = 1;
		Date max = *this;
		Date min = d;
		if (*this < d)
		{
			max = d;
			min = *this;
			flag = -1;
		}
		int day = 0;
		while (min != max)
		{
			min++;
			day++;
		}
		return day * flag;
	}

	Date operator++(int)//后置++
	{
		Date temp = *this;
		*this += 1;
		return temp;
	}

	Date& operator++()//前置++
	{
		*this += 1;
		return *this;
	}

	Date& operator--()//前置--
	{
		*this -= 1;
		return *this;
	}

	Date operator--(int)//后置--
	{
		Date temp = *this;
		*this -= 1;
		return temp;
	}

	bool operator==(const Date& d)const
	{
		return _year == d._year
			&& _month == d._month
			&& _day == d._day;
	}

	bool operator!=(const Date& d)const
	{
		return !(*this == d);
	}

	bool operator>(const Date& d)const
	{
		if (_year > d._year)
		{
			return true;
		}
		else if (_year == d._year)
		{
			if (_month > d._month)
			{
				return true;
			}
			else if (_month == d._month)
			{
				if (_day > d._day)
				{
					return true;
				}
			}
		}
		return false;
	}

	bool operator>=(const Date& d)const
	{
		return (*this > d) || (*this) == d;
	}

	bool operator<(const Date& d)const
	{
		return !(*this >= d);
	}

	bool operator<=(const Date& d)const
	{
		return !(*this > d);
	}

	Date* operator&()
	{
		return this;
	}

	const Date* operator&()const
	{
		return this;
	}

	friend ostream& operator<<(ostream& out, Date& d);
	friend istream& operator>>(istream& in, Date& d);

private:
	int _year;
	int _month;
	int _day;
};

ostream& operator<<(ostream& out, Date& d)
{
	cout << d._year << "年" << d._month << "月" << d._day << "日" << endl;
	return out;
}

istream& operator>>(istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;
	return in;
}

int main()
{
	Date d1(2022, 2, 3);
	d1 -= 31;
	d1.Print();
	return 0;
}