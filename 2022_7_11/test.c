#pragma warning(disable:4996)
#include<iostream>
#include<string.h>
#include<algorithm>
using namespace std;
using std::cout;
using std::endl;
namespace ds
{
	class string
	{
	public:
		typedef char* iterator;
		typedef const char* const_iterator;
		static size_t npos;

		string(const char* str= "")
		{
			_size = _capacity = strlen(str);
			_str = new char[_capacity + 1];
			strncpy(_str, str, _capacity + 1);
		}

		//string(const string& s)//传统写法
		//{
		//	_str = new char[strlen(s._str) + 1];
		//	strncpy(_str, s._str, strlen(s._str) + 1);
		//	_size = s._size;
		//	_capacity = s._capacity;
		//}
		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* str = new char[n + 1];
				strcpy(str, _str);
				delete[]_str;
				_str = str;
			}
		}

		//遍历string三种方式
		char& operator[](size_t pos)
		{
			return _str[pos];
		}

		const char& operator[](size_t pos)const
		{
			return _str[pos];
		}

		iterator begin()
		{
			return _str;
		}

		const_iterator begin()const
		{
			return _str;
		}


		iterator end()
		{
			return _str + _size;
		}

		const_iterator end()const
		{
			return _str + _size;
		}

		//增
		void push_back(const char ch)
		{
			size_t newcapacity = _capacity == 0 ? 4 : 2 * _capacity;
			if (_size == _capacity)
			{
				reserve(newcapacity);
			}
			_capacity = newcapacity;
			_str[_size] = ch;
			_str[_size + 1] = '\0';
			_size++;
		}

		void append(const char* str)
		{
			int len = strlen(str) + _size;
			reserve(len);
			strcpy(_str + _size, str);
		}

		string& operator+=(const char* str)
		{
			append(str);
			return *this;
		}

		string& operator+=(const char ch)
		{
			push_back(ch);
			return *this;
		}

		//删
		string& erase(size_t pos = 0, size_t len = npos)
		{
			if (len > _size - pos)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				strcpy(_str + pos, _str + pos + len);
			} 
			return *this;
		}

		//查
		size_t find(const char* str,size_t pos)const
		{
			char* start = _str + pos;
			char* end = strstr(start, str);
			if (end != nullptr)
			{
				return end - start;
			}
			return npos;
		}

		size_t find(char ch, size_t pos = 0)const
		{
			for (size_t i = 0; i < _size; i++)
			{
				if (ch == _str[i])
				{
					return i;
				}
			}
			return npos;
		}

		//改
		string& insert(size_t pos, const char* str)
		{
			int len = strlen(str);
			reserve(len + _size);
			char* end = _size + _str;
			while (end >= _str + pos)
			{
				end[len] = end[0];
				end--;
			}
			strncpy(_str + pos, str, len);
			_size += len;
			return *this;
		}

		string& insert(size_t pos, char ch)
		{
			size_t new_capacity = _capacity == 0 ? 4 : 2 * _capacity;
			if (_size = _capacity)
			{
				reserve(new_capacity);
				_capacity = new_capacity;
			}
			size_t end = _size;
			while (end >= pos)
			{
				_str[end + 1] = _str[end];
				end--;
			}
			_str[pos] = ch;
			_size++;
			return *this;
		}


		void swap(string& temp)
		{
			::swap(_str, temp._str);
			::swap(_size, temp._size);
			::swap(_capacity, temp._capacity);
		}

		string(const string& s)//现代写法
			:_str(nullptr)
		{
			string temp(s._str);
			swap(temp);
		}

		~string()
		{
			free(_str);
			_str = nullptr;
			_size = _capacity = 0;
		}

		const char* c_str()const
		{
			return _str;
		}

		//string& operator=(const string& s)//传统写法
		//{
		//	delete[]_str;
		//	_str = new char[strlen(s._str) + 1];
		//	strcpy(_str, s._str);
		//	_size = s._size;
		//	_capacity = s._capacity;
		//	return *this;
		//}

		string& operator=(string s)//现代写法
		{
			swap(s);
			return *this;
		}

		size_t size()const
		{
			return _size;
		}

		size_t capacity()const
		{
			return _capacity;
		}

		void clear()
		{
			_str[0] = '\0';
			_size = 0;
		}

		bool operator<(string& s)
		{
			return strcmp(_str, s._str) < 0;
		}
		bool operator==(string& s)
		{
			return strcmp(_str, s._str) == 0;
		}
		bool operator!= (string & s)
		{
			return !(*this == s);
		}
		bool operator<=(string& s)
		{
			return *this < s || *this == s;
		}
		bool operator>(string& s)
		{
			return !(*this <= s);
		}
		bool operator>=(string& s)
		{
			return *this > s || *this == s;
		}
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};
	
	size_t string::npos = -1;

	ostream& operator<<(ostream& out, const string& s)
	{
		out << s.c_str();
		return out;
	}

	istream& operator>>(istream& in, string& s)
	{
		s.clear();
		int ch = 0;
		while ((ch = getchar()) && ch != ' ' && ch != '\n')
		{
			s += ch;
		}
		return in;
	}

	//istream getline(istream in, string& s)
	//{
	//	int ch = 0;
	//	while ((ch = getchar()) != '\n')
	//	{
	//		s += ch;
	//	}
	//	return in;
	//}
	
}

//
//void string_test1()
//{
//	ds::string s1("hello world");
//	cout << s1 << endl;
//
//	ds::string s2;
//	s2 = s1;
//	cout << s2 << endl;
//}
//
//
//
//void string_test2()
//{
//	ds::string s1("hello world");
//	cin >> s1;
//	cout << s1 << endl;
//}

//void string_test3()
//{
//	//下表+[]
//	/*ds::string s("abcdef g!!!");
//	for (size_t i = 0; i < s.size(); i++)
//	{
//		cout << s[i];
//	}
//	cout << endl;*/
//
//	//范围for，或者叫foreach
//	//ds::string s("!!!!!!!!!!!!!asd");
//	//for (auto& x : s)
//	//{
//	//	cout << x;
//	//}
//	//cout << endl;
//
//	//ds::string s("sadasdw!!sadsa");
//	//////迭代器
//	//ds::string::iterator it = s.begin();
//	//while (it != s.end())
//	//{
//	//	cout << *it;
//	//	it++;
//	//}
//	//cout << endl;
//}

//void string_test4()
//{
//	//ds::string s("abc");
//	//s += 'd';
//	//cout << s << endl;
//	//s += "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!efg";
//	//cout << s << endl;
//	//ds::string s("abc");
//	//s.push_back('#');
//	//cout << s << endl;
//	//s.append("efg");
//	//cout << s << endl;
//
//}

//void string_test5()
//{
//	ds::string s("hello world");
//	s.erase(0, 2);
//	cout << s << endl;
//
//}

//void string_test6()
//{
//	ds::string s("hello world");
//	size_t pos = s.find("wa", 0);
//	//size_t pos = s.find('o', 0);
//	cout << pos << endl;
//}

//void string_test7()
//{
//	ds::string s("hello world");
//	size_t pos = s.find('h', 0);
//	s.insert(pos, "###");
//	cout << s << endl;
//}

int main()
{
	//string_test1();
	//string_test2();
	//string_test3();
	//string_test4();
	//string_test5();
	//string_test6();
	//string_test7();
	return 0;
}