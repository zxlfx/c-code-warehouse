#include "student_sys.h"

void menu()
{
	printf("******************************************\n");
	printf("*************学生成绩管理系统*************\n");
	printf("**************1.增加学生信息**************\n");
	printf("**************2.删除学生信息**************\n");
	printf("**************3.查找学生信息**************\n");
	printf("**************4.修改学生信息**************\n");
	printf("**************5.显示学生信息**************\n");
	printf("**************6.排序学生信息**************\n");
	printf("**************0.退出学生系统**************\n");
	printf("******************************************\n");
}

int main()
{
	int n = 0;
	System sms;
	S_Init(&sms);
	do
	{
		menu();
		printf("请输入你的选择:>");
		scanf("%d", &n);
		switch (n)
		{
		case 1:
		{
			S_Add(&sms);
			break;
		}

		case 2:
		{
			break;
		}

		case 3:
		{
			break;
		}

		case 4:
		{
			break;
		}

		case 5:
		{
			S_Show(&sms);
			break;
		}

		case 6:
		{
			break;
		}

		case 0:
		{
			break;
		}

		default:
		{
			printf("没有对应的选项，请重新输入\n");
			break;
		}
		}
	} while (n);


	return 0;
}