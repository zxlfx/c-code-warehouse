#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<thread>
#include<mutex>
#include<Windows.h>

using namespace std;

//void func(int n)
//{
//	for (int i = 0; i < 3; i++)
//	{
//		cout << i << " ";
//		Sleep(1000);
//	}
//	cout << endl;
//}
//
//int main()
//{
//	thread t(func, 10);
//	t.join();
//	cout << "abc" << endl;
//	return 0;
//}

//class A
//{
//public:
//	A()
//	{
//		cout << "A()" << endl;
//	}
//
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//};

mutex mtx;
condition_variable cv;
bool flag = false;

void printold(int n, int& i)
{
	while (1)
	{
		unique_lock<mutex> lock(mtx);
		if (i < n)
		{
			cv.wait(lock, [] {return !flag; });
			if (i < n)
			{
				cout << this_thread::get_id() << ":" << i << endl;
				i++;
				cv.notify_one();
				flag = true;
			}
			else
			{
				break;
			}
		}
		else
		{
			break;
		}
	}
}

void printeven(int n, int& i)
{
	while (1)
	{
		unique_lock<mutex> lock(mtx);
		if(i < n)
		{
			cv.wait(lock, [] {return flag; });
			if (i < n)
			{
				cout << this_thread::get_id() << ":" << i << endl;
				i++;
				cv.notify_one();
				flag = false;
			}
			else
			{
				break;
			}
		}
		else
		{
			break;
		}
	}
}
int main()
{
	int n = 0;
	int i = 1;
	cin >> n;
	thread t1(printold, n, ref(i));
	thread t2(printeven, n, ref(i));

	t1.join();
	t2.join();
}