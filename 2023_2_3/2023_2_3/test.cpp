#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;


class Person
{
public:
	~Person()
	{
		cout << "~Person" << endl;
	}
	Person(string name = "张三", string sex = "男", int id = 5)
		:_name(name),
		_sex(sex),
		_id(id)
	{}

	void print()
	{
		cout << "name:" << _name << " " << "sex:" << _sex << " " << "id:" << _id << endl;
	}
protected:
	string _name;
	string _sex;
	int _id;
};

class Student :public Person
{
public:
	Student()
		:_id(10)
	{}
protected:
	int _id;
};

int main()
{
	Person p;
	new(&p)Person("李四","女", 20);
	p.~Person();
	return 0;
}