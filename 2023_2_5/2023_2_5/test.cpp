#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;


class Person
{
public:
	virtual void Func1()
	{
		cout << "Person:void Func1()" << endl;
	}

	virtual void Func2()
	{
		cout << "Person::void Func2()" << endl;
	}

	virtual void Func3()
	{
		cout << "Person::void Func3()" << endl;
	}
};

class Student :public Person
{
public:
	virtual void Func1()
	{
		cout << "Student::void Func1()" << endl;
	}
	virtual void Func2()
	{
		cout << "Stduent::void Func2()" << endl;
	}
	virtual void Func4()
	{
		cout << "Student::void Func4()" << endl;
	}
};

typedef void(*VPTR)();
void PrintVFT(VPTR* p)
{
	for (int i = 0; p[i]; i++)
	{
		cout << "p[i]:"<<p[i]<<"->";
		p[i]();
		cout << endl;
	}
}
int main()
{
	Person p;
	Student s;
	PrintVFT((VPTR*)*(int*)&s);
	cout << "---------------------" << endl;
	PrintVFT((VPTR*)*(int*)&p);
	return 0;
}