#include<iostream>
#include<string>
using namespace std;


class people
{
public:
	~people()
	{
		cout << "~people()" << endl;
	}
private:
	int id;
	string sex;
};
void test1()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	for (auto& e : arr)
	{
		e *= 2;
	}
	for (auto e : arr)
	{
		cout << e << " ";
	}
	cout << endl;
}
int Add(int a = 1, int b = 2)
{
	return a + b;
}
void test2()
{
	int c = Add();
	cout << c << endl;
}
int main()
{
	people p1;
	return 0;
}