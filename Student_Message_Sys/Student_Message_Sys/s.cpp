#include"s.h"

Student& Student::operator=(Student& s)
{
	this->_name = s._name;
	this->_age = s._age;
	this->_id = s._id;
	this->_addr = s._addr;
	this->_sex = s._sex;
	this->_tel = s._tel;
	this->_identity = s._identity;
	return *this;
}

void System::Load()
{
	FILE* pr = fopen("data.txt", "r");
	if (pr == NULL)
	{
		return;
	}
	while (~fscanf(pr, "%s %lld %d %s %s %lld %s", _a[_size]._name.c_str(), &_a[_size]._id, &_a[_size]._age, \
		_a[_size]._sex.c_str(), _a[_size]._addr.c_str(), &_a[_size]._tel, _a[_size]._identity.c_str()))
	{
		_size++;
	}
	fclose(pr);
}

void System::Write()
{
	FILE* pw = fopen("data.txt", "w");

	for (int i = 0; i < _size; i++)
	{
		fprintf(pw, "%-10s %-15lld %-8d %-8s %-18s %-15lld %-8s\n", _a[i]._name.c_str(), _a[i]._id, _a[i]._age,
			_a[i]._sex.c_str(), _a[i]._addr.c_str(), _a[i]._tel, _a[i]._identity.c_str());
	}

	fclose(pw);
}

System::System()
{
	_capacity = 5;
	_size = 0;
	_a = new Student[_capacity];
	Load();
}

System::~System()
{
	//Write();
	delete[] _a;
	_a = nullptr;
}

void System::Add()
{
	if (_size == _capacity)
	{
		_capacity = 2 * _capacity;
		delete _a;
		_a = new Student[_capacity];
	}

	cout << "请输入姓名:>";
	cin >> _a[_size]._name;

	cout << "学号:>";
	cin >> _a[_size]._id;

	cout << "年龄:>";
	cin >> _a[_size]._age;

	cout << "性别:>";
	cin >> _a[_size]._sex;

	cout << "地址:>";
	cin >> _a[_size]._addr;

	cout << "电话:>";
	cin >> _a[_size]._tel;

	cout << "身份:>";
	cin >> _a[_size]._identity;

	_size++;
	cout << "添加成功！！！" << endl;
}


void System::Show()
{
	printf("%-10s %-15s %-8s %-8s %-18s %-15s %-8s\n", "姓名", "学号", "年龄", "性别", "地址", "电话", "身份");
	for (int i = 0; i < _size; i++)
	{
		printf("%-10s %-15lld %-8d %-8s %-18s %-15lld %-8s\n", _a[i]._name.c_str(), _a[i]._id,
			_a[i]._age, _a[i]._sex.c_str(), _a[i]._addr.c_str(), _a[i]._tel, _a[i]._identity.c_str());
	}

}

void System::Delete()
{
	int n = 0;
	int cnt = 0;
	cout << "1.按照名字删除   2.按学号删除" << endl;
	cin >> n;
	if (n == 1)
	{
		int flag = 0;
		cout << "请输入要删除人的名字:>";
		string s;
		cin >> s;
		for (int i = 0; i < _size; i++)
		{
			if (strcmp(s.c_str(), _a[i]._name.c_str()) == 0)
			{
				cnt++;
				flag = 1;
			}
		}

		if (flag == 0)
		{
			cout << "系统中无此人" << endl;
		}

		if (cnt == 1)
		{
			for (int i = 0; i < _size; i++)
			{

				if (strcmp(s.c_str(), _a[i]._name.c_str()) == 0)
				{
					printf("%-10s %-15lld %-8d %-8s %-18s %-15lld %-8s\n", _a[i]._name.c_str(), _a[i]._id,
						_a[i]._age, _a[i]._sex.c_str(), _a[i]._addr.c_str(), _a[i]._tel, _a[i]._identity.c_str());

					char n = 0;
					cout << "是否确实删除 Y/N" << endl;
					cin >> n;
					if (n == 'Y' || n == 'y')
					{
						int j = 0;
						while (j < _size)
						{
							_a[j] = _a[j + 1];
							j++;
						}
						_size--;
						cout << "删除成功！！" << endl;
						return;
					}
				}
			}
		}

		if (cnt > 1)
		{
			for (int i = 0; i < _size; i++)
			{

				if (strcmp(s.c_str(), _a[i]._name.c_str()) == 0)
				{
					printf("%-10s %-15lld %-8d %-8s %-18s %-15lld %-8s\n", _a[i]._name.c_str(), _a[i]._id,
						_a[i]._age, _a[i]._sex.c_str(), _a[i]._addr.c_str(), _a[i]._tel, _a[i]._identity.c_str());
				}
			}
			char n = 0;
			cout << "学生不止一个，是否全部删除 Y/N" << endl;
			cin >> n;
			if (n == 'Y' || n == 'y')
			{
				for (int i = 0; i < _size; i++)
				{

					if (strcmp(s.c_str(), _a[i]._name.c_str()) == 0)
					{
						int j = i;
						while (j < _size)
						{
							_a[j] = _a[j + 1];
							j++;
						}
					}
				}
				_size--;
				cout << "删除成功！！" << endl;
				return;
			}

		}

	}

	else if (n == 2)
	{
		cout << "请输入要删除人的学号：>";
		long long n;
		cin >> n;
		for (int i = 0; i < _size; i++)
		{

			if (n == _a[i]._id)
			{
				printf("%-10s %-15lld %-8d %-8s %-18s %-15lld %-8s\n", _a[i]._name.c_str(), _a[i]._id,
					_a[i]._age, _a[i]._sex.c_str(), _a[i]._addr.c_str(), _a[i]._tel, _a[i]._identity.c_str());
				char n = 0;
				cout << "是否确实删除 Y/N" << endl;
				cin >> n;
				if (n == 'Y' || n == 'y')
				{
					int j = i;
					while (j < _size)
					{
						_a[j] = _a[j + 1];
						j++;
					}
					_size--;
					cout << "删除成功！！" << endl;
					return;
				}
			}
		}
		cout << "系统中无此人" << endl;
	}

	else
	{
		cout << "无该选项" << endl;
	}
}

void System::Search()
{
	int n = 0;
	int flag = 0;
	cout << "1.按名字查找   2.按学号查找" << endl;
	cin >> n;
	if (n == 1)
	{
		cout << "请输入要查找人的名字" << endl;
		string s;
		cin >> s;

		for (int i = 0; i < _size; i++)
		{

			if (strcmp(s.c_str(), _a[i]._name.c_str()) == 0)
			{
				printf("%-10s %-15lld %-8d %-8s %-18s %-15lld %-8s\n", _a[i]._name.c_str(), _a[i]._id,
					_a[i]._age, _a[i]._sex.c_str(), _a[i]._addr.c_str(), _a[i]._tel, _a[i]._identity.c_str());
				flag = 1;
			}
		}
		if (flag == 0)
		{
			cout << "系统中无此人" << endl;
		}
	}

	else if (n == 2)
	{
		long long n = 0;
		cout << "请输入要查找人的id" << endl;
		cin >> n;
		for (int i = 0; i < _size; i++)
		{

			if (n == _a[i]._id)
			{
				printf("%-10s %-15lld %-8d %-8s %-18s %-15lld %-8s\n", _a[i]._name.c_str(), _a[i]._id,
					_a[i]._age, _a[i]._sex.c_str(), _a[i]._addr.c_str(), _a[i]._tel, _a[i]._identity.c_str());
				return;
			}
		}
		cout << "系统中无此人" << endl;

	}

	else
	{
		cout << "无该选项" << endl;
	}
}


void System::Modify()
{
	int n = 0;
	int cnt = 0;
	cout << "1.按名字修改  2.按学号修改" << endl;
	cin >> n;

	if (n == 1)
	{
		int flag = 0;
		cout << "请输入要修改人的名字:>";
		string s;
		cin >> s;
		for (int i = 0; i < _size; i++)
		{
			if (strcmp(s.c_str(), _a[i]._name.c_str()) == 0)
			{
				cnt++;
				flag = 1;
			}
		}

		if (flag == 0)
		{
			cout << "系统中无此人" << endl;
		}

		if (cnt == 1)
		{
			for (int i = 0; i < _size; i++)
			{
				if (strcmp(s.c_str(), _a[i]._name.c_str()) == 0)
				{
					printf("%-10s %-15lld %-8d %-8s %-18s %-15lld %-8s\n", _a[i]._name.c_str(), _a[i]._id,
						_a[i]._age, _a[i]._sex.c_str(), _a[i]._addr.c_str(), _a[i]._tel, _a[i]._identity.c_str());

					cout << "请输入姓名:>";
					cin >> _a[i]._name;

					cout << "学号:>";
					cin >> _a[i]._id;

					cout << "年龄:>";
					cin >> _a[i]._age;

					cout << "性别:>";
					cin >> _a[i]._sex;

					cout << "地址:>";
					cin >> _a[i]._addr;

					cout << "电话:>";
					cin >> _a[i]._tel;

					cout << "身份:>";
					cin >> _a[i]._identity;

					cout << "修改成功！！" << endl;
					return;
				}
			}
		}

		if (cnt > 1)
		{
			cout << "拥有该名字的人不唯一，无法修改" << endl;
		}
	}

	else if (n == 2)
	{
		long long id;
		cout << "请输入要修改人的id" << endl;
		cin >> id;
		for (int i = 0; i < _size; i++)
		{
			if (id == _a[i]._id)
			{
				printf("%-10s %-15lld %-8d %-8s %-18s %-15lld %-8s\n", _a[i]._name.c_str(), _a[i]._id,
					_a[i]._age, _a[i]._sex.c_str(), _a[i]._addr.c_str(), _a[i]._tel, _a[i]._identity.c_str());

				cout << "请输入姓名:>";
				cin >> _a[i]._name;

				cout << "学号:>";
				cin >> _a[i]._id;

				cout << "年龄:>";
				cin >> _a[i]._age;

				cout << "性别:>";
				cin >> _a[i]._sex;

				cout << "地址:>";
				cin >> _a[i]._addr;

				cout << "电话:>";
				cin >> _a[i]._tel;

				cout << "身份:>";
				cin >> _a[i]._identity;

				cout << "修改成功！！" << endl;
				return;
			}
		}
		cout << "系统中无此人" << endl;
		return;
	}

	else
	{
		cout << "无该选项" << endl;
	}
}



void System::Screen()
{
	int n = 0;
	cout << "1.筛选团员  2.筛选党员  3.筛选群众" << endl;
	cin >> n;

	if (n == 1)
	{
		int flag = 0;
		for (int i = 0; i < _size; i++)
		{
			if (strcmp(_a[i]._identity.c_str(), "团员") == 0)
			{
				printf("%-10s %-15lld %-8d %-8s %-18s %-15lld %-8s\n", _a[i]._name.c_str(), _a[i]._id,
					_a[i]._age, _a[i]._sex.c_str(), _a[i]._addr.c_str(), _a[i]._tel, _a[i]._identity.c_str());
				flag = 1;
			}
		}
		if (flag == 0)
		{
			cout << "无团员" << endl;
		}
	}

	else if (n == 2)
	{
		int flag = 0;
		for (int i = 0; i < _size; i++)
		{
			if (strcmp(_a[i]._identity.c_str(), "党员") == 0)
			{
				printf("%-10s %-15lld %-8d %-8s %-18s %-15lld %-8s\n", _a[i]._name.c_str(), _a[i]._id,
					_a[i]._age, _a[i]._sex.c_str(), _a[i]._addr.c_str(), _a[i]._tel, _a[i]._identity.c_str());
				flag = 1;
			}
		}

		if (flag == 0)
		{
			cout << "无党员" << endl;
		}
	}

	else if (n == 3)
	{
		int flag = 0;
		for (int i = 0; i < _size; i++)
		{
			if (strcmp(_a[i]._identity.c_str(), "群众") == 0)
			{
				printf("%-10s %-15lld %-8d %-8s %-18s %-15lld %-8s\n", _a[i]._name.c_str(), _a[i]._id,
					_a[i]._age, _a[i]._sex.c_str(), _a[i]._addr.c_str(), _a[i]._tel, _a[i]._identity.c_str());

				flag = 1;
			}
		}
		if (flag == 0)
		{
			cout << "无群众" << endl;
		}
	}

	else
	{
		cout << "无该选项" << endl;
	}
}