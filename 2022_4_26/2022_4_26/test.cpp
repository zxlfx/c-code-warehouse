#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using std::cout;
using std::endl;
class A
{
public:
	A()
	{
		cout << "无参构造" << endl;
	}
	A(const A& a)
	{
		cout << "拷贝构造" << endl;
	}
};
A f(A a)
{
	printf("&&&\n");
	return a;
}
int main()
{
	A a1;
	A a2=f(a1);
}