#include<iostream>
#include<string>
#include<vector>
using namespace std;

//int main()
//{
//    string s;
//    getline(cin, s);
//    int pos = s.rfind(' ');
//    cout << strlen(s.c_str() + pos + 1) << endl;
//    return 0;
//}

//class Solution {
//public:
//    bool isPalindrome(string s)
//    {
//        int end = s.size() - 1;
//        int start = 0;
//
//        for (auto& e : s)
//        {
//            if (e >= 'a' && e <= 'z')
//            {
//                e -= 32;
//            }
//        }
//
//        while (end > start)
//        {
//            while (end > start && !isalpha(s[end]))
//            {
//                end--;
//            }
//            while (end > start && !isalpha(s[start]))
//            {
//                start++;
//            }
//            if (s[end] != s[start])
//            {
//                return false;
//            }
//            end--;
//            start++;
//        }
//        return true;
//    }
//};


/*class Solution {
public:

    void _reverseStr(string s, int k)
    {
        if (s.size() < k)
        {
            reverse(s.begin(), s.end());
            return;
        }

        if (s.size() < 3 * k)
        {
            reverse(s.begin(), s.begin() + k);
            reverse(s.begin() + 2 * k, s.end());
            return;
        }

        if (s.size() - 2 * k < 2 * k && s.size() - 2 * k >= k)
        {
            reverse(s.begin(), s.begin() + k);
            reverse(s.begin() + 2 * k, s.begin() + 2 * k + k);
            return;
        }

        s = s.c_str() + 2 * k;
        _reverseStr(s, k);
    }
    string reverseStr(string s, int k)
    {
        _reverseStr(s, k);
        return s;
    }
}*/;

//
//class Solution {
//public:
//    string reverseWords(string s) {
//        string::iterator begin = s.begin();
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (s[i] == ' ')
//            {
//                reverse(begin, begin + i);
//                begin = s.begin() + i + 1;
//            }
//        }
//        return s;
//    }
////};
//
//
//class Solution {
//public:
//    string multiply(string num1, string num2)
//    {
//        int n = num1.size() + num2.size() - 1;
//        vector<int> a(n, 0);
//        int i = 0;
//        int j = 0;
//        string s;
//
//        for (i = 0; i < num1.size(); i++)
//        {
//            for (j = 0; j < num2.size(); j++)
//            {
//                a[i + j] += (num1[i] - '0') * (num2[i] - '0');
//            }
//        }
//
//        for (i = n - 1; i >= 1; i--)
//        {
//            a[i - 1] += a[i] / 10;
//            a[i] %= 10;
//        }
//
//        for (i = 0; i < n; i++)
//        {
//            s += to_string(s[i]);
//        }
//        return s;
//    }
//};


class Solution {
public:
    string multiply(string num1, string num2)
    {
        if (num1[0] == '0' || num2[0] == '0')
        {
            return "0";
        }
        int n = num1.size() + num2.size() - 1;
        vector<int> a(n, 0);
        string s;
        for (int i = 0; i < num1.size(); i++)
        {
            for (int j = 0; j < num2.size(); j++)
            {
                a[i + j] += (num1[i] - '0') * (num2[j] - '0');
            }
        }
        for (int i = n - 1; i >= 1; i--)
        {
            a[i - 1] += a[i] / 10;
            a[i] %= 10;
        }

        for (int i = 0; i < n; i++)
        {
            s += to_string(a[i]);
        }
        return s;
    }
};



int main()
{
    cout << Solution().multiply("123", "99") << endl;
    return 0;
}
