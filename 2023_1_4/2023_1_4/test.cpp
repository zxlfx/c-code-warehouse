#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<list>
#include<time.h>
#include<algorithm>
#include<cstdlib>

using namespace std;

void Test()
{
	const int N = 1000000;
	vector<int> v;
	list<int> lt;
 	for (int i = 0; i < N; i++)
	{
		v.push_back(rand());
		lt.push_back(v[i]);
	}

	int begin1 = clock();
	sort(v.begin(), v.end());
	int end1 = clock();

	int begin2 = clock();
	lt.sort();
	int end2 = clock();
	cout << "vector sort: " << end1 - begin1 << endl;
	cout << "list sort: " << end2 - begin2 << endl;
}
int main()
{
	srand(time(0));
	Test();
	return 0;
}