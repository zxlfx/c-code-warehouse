#pragma once
#include<iostream>
#include<string>
#include<assert.h>
#include"reverse_iterator.h"

using namespace std;
namespace ds
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;
		typedef Reverse_iterator<iterator> reverse_iterator;
		typedef Reverse_iterator<const_iterator> const_reverse_iterator;


		iterator begin()
		{
			return _start;
		}

		const_iterator begin()const
		{
			return _start;
		}

		iterator end()
		{
			return _finish;
		}

		const_iterator end()const
		{
			return _finish;
		}


		reverse_iterator rbegin()
		{
			return end();
		}

		const_reverse_iterator rbegin()const
		{
			return end();
		}

		reverse_iterator rend()
		{
			return begin();
		}

		const_reverse_iterator rend()const
		{
			return begin();
		}

		vector()
			:_start(nullptr),
			_finish(nullptr),
			_end_of_storage(nullptr)
		{
			;
		}

		template<class InputIterator>
		vector(InputIterator first, InputIterator last)
			:_start(nullptr),
			_finish(nullptr),
			_end_of_storage(nullptr)
		{
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}
		
		//vector(const vector<T>& v)//传统写法
		//	:_start(nullptr),
		//	_finish(nullptr),
		//	_end_of_storage(nullptr)
		//{
		//	for (auto s : v)
		//	{
		//		push_back(s);
		//	}
		//}


		vector(const vector<T>& v)//现代写法
			:_start(nullptr),
			_finish(nullptr),
			_end_of_storage(nullptr)
		{
			vector<T> temp(v.begin(), v.end());
			swap(temp);
		}

		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_end_of_storage, v._end_of_storage);
		}

		~vector()
		{
			if (_start)
			{
				delete[] _start;
				_start = _finish = _end_of_storage = nullptr;
			}
		}

		/*vector<T>& operator=(const vector<T>& v)//传统写法
		{
			delete[] _start;
			_start = _finish = _end_of_storage = nullptr;
			for (auto s : v)
			{
				push_back(s);
			}
			return *this;
		}*/

		vector<T>& operator=(vector<T> v)//现代写法
		{
			swap(v);
			return *this;
		}

		void reserve(size_t n)
		{
			if (n > capacity())
			{
				int i = 0;
				size_t sz = size();
				T* temp = new T[n];
				while (i < sz)
				{
					temp[i] = _start[i];
					i++;
				}
				delete[] _start;
				_start = temp;
				_finish = temp + sz;
				_end_of_storage = temp + n;
			}
		}

		void resize(size_t n, const T& x = T())
		{
			if (n < size())
			{
				_finish = _start + n;
			}
			else
			{
				if (n > capacity())
				{
					reserve(n);
				}

				while(_finish < _start + n)
				{
					*_finish = x;
					_finish++;
				}
			}
		}

		void push_back(const T& x)
		{
			if (_finish == _end_of_storage)
			{
				reserve(capacity() == 0 ? 4 : capacity() * 2);
			}
			*_finish = x;
			_finish++;
		}

		T& operator[](size_t pos)
		{
			assert(pos < size());
			return _start[pos];
		}

		const T& operator[](size_t pos)const
		{
			assert(pos < size());
			return _start[pos];
		}

		size_t capacity()const
		{
			return _end_of_storage - _start;
		}

		size_t size()const
		{
			return _finish - _start;
		}

		bool empty()const
		{
			return _finish == _start;
		}

		void clear()
		{
			_finish = _start;
		}

		iterator insert(iterator pos, const T& x)
		{
			assert(pos>=_start && pos <= _finish);
			if (_finish == _end_of_storage)
			{
				size_t n = pos - _start;
				reserve(capacity() == 0 ? 4 : 2 * capacity());
				pos = _start + n;
			}

			T* end = _finish - 1;
			T* start = pos;
			while (end >= start)
			{
				*(end + 1) = *end;
				end--;
			}
			*pos = x;
			_finish++;
			return pos;
		}

		iterator earse(iterator pos)
		{
			assert(pos>=_start && pos < _finish);
			T* start = pos;
			T* end = _finish;
			while (start < end)
			{
				*start = *(start + 1);
				start++;
			}
			_finish--;
			return pos;
		}

	private:
		T* _start;
		T* _finish;
		T* _end_of_storage;
	};


	void test_vector1()//构造函数，赋值重载
	{
		ds::vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);

		for (size_t i = 0; i < v.size(); i++)//下标+[]
		{
			cout << v[i] << " ";
		}
		cout << endl;

		ds::vector<int>::iterator it = v.begin();//迭代器
		while (it != v.end())
		{
			cout << *it << " ";
			it++;
		}
		cout << endl;

		for (auto e : v)//范围for
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test_vector2()
	{
		ds::vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(1);
		v.push_back(1);
		v.push_back(1);
		v.push_back(1);

		for (auto e : v)//范围for
		{
			cout << e << " ";
		}
		cout << endl;


		vector<int> v1;
		v1 = v;
		for (auto e : v1)//范围for
		{
			cout << e << " ";
		}
		cout << endl;

	}

	void test_vector3()
	{
		ds::vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);

		const vector<int> v1(v.begin(), v.end());
		auto it = v1.begin();
		while (it != v1.end())
		{
			//*it = 3;
			cout << *it << " ";
			it++;
		}
		cout << endl;

	}

	void test_vector4()
	{
		ds::vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);
		v.reserve(100);
		v.resize(4);
	}

	void test_vector5()
	{
		ds::vector<string> v;
		v.push_back("aaa");
		v.push_back("bbb");
		v.push_back("ccc");
		v.push_back("ddd");
		v.push_back("eee");
		v.push_back("fff");

		ds::vector<string>v1(v);
		for (auto e : v1)//范围for
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test_vector6()
	{
		ds::vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);

		for (auto e : v)//范围for
		{
			cout << e << " ";
		}
		cout << endl;

		//v.insert(v.begin(), 10);
		//v.insert(v.end(), 50);

		v.earse(v.begin());

		for (auto e : v)//范围for
		{
			cout << e << " ";
		}
		cout << endl;

		v.earse(v.begin() + 2);

		for (auto e : v)//范围for
		{
			cout << e << " ";
		}
		cout << endl;


	}

	void test_vector7()
	{
		ds::vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);

		const ds::vector<int> v1(v.begin(), v.end());
		auto it = v1.rbegin();
		while (it != v1.rend())
		{
			//*it += 1;
			cout << *it << " ";
			it++;
		}
		cout << endl;
	}
}

