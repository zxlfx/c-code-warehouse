#define _CRT_SECURE_NO_WARNINGS 1
#include"vector.h"
#include<vector>

//void test()
//{
//	vector<int> v{ 1,2,3,4,5,6 };
//	auto it = v.begin();
//	while (it != v.end())
//	{
//		if (*it % 2 == 0)
//		{
//			it = v.insert(it, (*it) * 10);
//			it += 2;
//		}
//		else
//		{
//			it++;
//		}
//	}
//
//	for (auto e : v)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//}

void test()
{
	vector<int> v{ 1,2,2,4,5,6 };
	auto it = v.begin();
	while (it != v.end())
	{
		if (*it % 2 == 0)
		{
			it = v.erase(it);
		}
		else
		{
			it++;
		}
	}

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}

int main()
{
	test();
	return 0;
}

//int main()
//{
//	//ds::test_vector1();
//	//ds::test_vector2();
//	//ds::test_vector5();
//	//ds::test_vector6();
//	//ds::test_vector7();
//
//	return 0;
//}