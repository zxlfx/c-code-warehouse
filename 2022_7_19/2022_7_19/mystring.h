#pragma once
#include<iostream>
#include<cstring>
#include<assert.h>
namespace ds
{
	class string
	{
	public:
		typedef char* iterator;
		typedef const char* const_iterator;
		static size_t npos;
		string(const char* str = "")
			:_size(strlen(str)),
			_capacity(_size)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}

		~string()
		{
			if (_str)
			{
				delete[] _str;
				_str = nullptr;
				_size = _capacity = 0;
			}
		}

		//string(const string& s)//传统写法
		//{
		//	_str = new char[s._capacity + 1];
		//	_capacity = s._capacity;
		//	_size = s._size;
		//	strcpy(_str, s._str);
		//}

		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}

		string(const string& s)//现代写法
			:_str(nullptr),
			_size(0),
			_capacity(0)
		{
			string temp(s._str);
			swap(temp);
		}

		//string& operator=(const string& s)//传统写法
		//{
		//	if (this != &s)
		//	{
		//		char* temp = new char[s._capacity + 1];
		//		strcpy(temp, s._str);
		//		delete[] _str;
		//		_str = temp;
		//		_size = s._size;
		//		_capacity = s._capacity;
		//	}
		//	return *this;
		//}

		string& operator=(string s)//现代写法
		{
			swap(s);
			return *this;
		}


		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* temp = new char[n + 1];
				strcpy(temp, _str);
				delete[] _str;
				_str = temp;
				_capacity = n;
			}
		}

		void resize(size_t n, char ch = '\0')
		{
			if (n > _capacity)
			{
				reserve(n);
				for (size_t i = _size; i < n; i++)
				{
					_str[i] = ch;
				}
				_str[n] = '\0';
				_size = n;
			}
			else
			{
				_str[n] = '\0';
				_size = n;
			}
		}

		void push_back(char ch)
		{
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : 2 * _capacity);
			}
			_str[_size] = ch;
			_size++;
			_str[_size] = '\0';
		}

		void pop_back()
		{
			_size--;
			_str[_size] = '\0';
		}

		void append(const char* str)
		{
			size_t n = _size + strlen(str);
			if (n > _capacity)
			{
				reserve(n);
			}
			strcpy(_str + _size, str);
			_size = n;
		}

		const char* c_str()const
		{
			return _str;
		}

		size_t size()const
		{
			return _size;
		}

		size_t capacity()const
		{
			return _capacity;
		}

		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		const_iterator begin()const
		{
			return _str;
		}

		const_iterator end()const
		{
			return _str + _size;
		}

		bool empty()const
		{
			return _size == 0;
		}

		void clear()
		{
			_size = 0;
			_str[0] = '\0';
		}
		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}

		const char& operator[](size_t pos)const
		{
			assert(pos < _size);
			return _str[pos];
		}

		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}

		string& operator+=(const char* str)
		{
			append(str);
			return *this;
		}

		string& insert(size_t pos, char ch)
		{
			assert(pos <= _size);
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}
			char* start = _str + pos;
			char* end = _str + _size;
			while (end >= start)
			{
				*(end + 1) = *end;
				end--;
			}
			_str[pos] = ch;
			_size++;
			return *this;
		}

		string& insert(size_t pos, const char* str)
		{
			assert(pos <= _size);
			size_t size = strlen(str);
			if (_size + size > _capacity)
			{
				reserve(_size + size);
			}
			char* start = _str + pos;
			char* end = _str + _size;
			while (end >= start)
			{
				*(end + size) = *end;
				end--;
			}
			strncpy(_str + pos, str, size);
			_size += size;
			return *this;
		}

		string& erase(size_t pos, int n = npos)
		{
			assert(pos < _size);
			if (pos + n >= _size)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				char* start = _str + pos;
				char* end = _str + _size;
				while (start < end)
				{
					*start = *(start + n);
					start++;
				}
				_size -= n;
			}
			return *this;
		}


		size_t find(char ch, size_t pos = 0)
		{
			for (size_t i = pos; i < size(); i++)
			{
				if (_str[i] == ch)
				{
					return i;
				}
			}
			return npos;
		}

		size_t find(const char* str, size_t pos = 0)
		{
			char* ret = strstr(_str + pos, str);
			if (ret != nullptr)
			{
				return ret - _str;
			}
			return npos;
		}

	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};

	size_t string::npos = -1;


	std::ostream& operator<<(std::ostream& out, const string& s)
	{
		for (auto& a : s)
		{
			out << a;
		}
		return out;
	}

	std::istream& operator>>(std::istream& in, string& s)
	{
		s.clear();
		int ch = 0;
		while ((ch = in.get()) != ' ' && ch != '\n')
		{
			s.push_back(ch);
		}
		return in;
	}
	

	bool operator>(const string& s1, const string& s2)
	{
		return strcmp(s1.c_str(), s2.c_str()) > 0;
	}

	bool operator==(const string& s1, const string& s2)
	{
		return strcmp(s1.c_str(), s2.c_str()) == 0;
	}

	bool operator>=(const string& s1, const string& s2)
	{
		return operator>(s1, s2) || operator==(s1, s2);
	}

	bool operator<(const string& s1, const string& s2)
	{
		return !operator>=(s1, s2);
	}

	bool operator<=(const string& s1, const string& s2)
	{
		return !operator>(s1, s2);
	}

	bool operator!=(const string& s1, const string& s2)
	{
		return !operator==(s1, s2);
	}
}


	

void test_string1()//构造、拷贝构造、赋值
{
	ds::string s1;
	ds::string s2("hello world");
	ds::string s3(s2);
	std::cout << s1 << std::endl;
	std::cout << s2 << std::endl;
	std::cout << s3 << std::endl;
	s1 = s3;
	std::cout << s1 << std::endl;
}

void test_string2()//遍历
{
	//下标+[]
	ds::string s("hello world");
	const ds::string cs("aaaaaaaa");

	for (size_t i = 0; i < cs.size(); i++)
	{
		//cs[i] = 3;
		std::cout << cs[i] << " ";
	}
	std::cout << std::endl;

	//迭代器
	ds::string::const_iterator it = cs.begin();
	while (it != cs.end())
	{
		//*it = 3;
		std::cout << *it << " ";
		it++;
	}
	std::cout << std::endl;


	//范围for
	for (const auto& c : cs)
	{
		//c = 3;
		std::cout << c << " ";
	}
	std::cout << std::endl;

}


void test_string3()
{
	//ds::string s("hello");
	//s.push_back(' ');
	//s.append("world");
	ds::string s;
	s += "hello";
	s += " world";
	std::cout << s << std::endl;
}

void test_string4()
{
	ds::string s("hello world");
	//s.reserve(100);
	s.resize(5, 'x');
}

void test_string5()
{
	ds::string s("hello world");
	s.insert(0, 'x');
	std::cout << s << std::endl;;

	s.insert(3, "haha");
	std::cout << s << std::endl;;

	s.erase(0);
	std::cout << s << std::endl;
}

void test_string6()
{
	ds::string s;
	std::cin >> s;
	std::cout << s << std::endl;
}

void test_string7()
{
	ds::string s("hello world");
	size_t pos1 = s.find('2');
	if (pos1 != ds::string::npos)
	{
		std::cout << s.c_str() + pos1 << std::endl;
	}
	else
	{
		std::cout << "找不到" << std::endl;
	}

	size_t pos2 = s.find("wor");
	if (pos2 != ds::string::npos)
	{
		std::cout << s.c_str() + pos2 << std::endl;
	}
}