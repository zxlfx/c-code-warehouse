#include<iostream>
#include<string>
#include<vector>
using namespace std;

//class Solution {
//public:
//    string multiply(string num1, string num2)
//    {
//        return to_string(atoi(num1.c_str()) * atoi(num2.c_str()));
//    }
//};

class Solution {
public:
    string multiply(string num1, string num2)
    {
        if (num1[0] == '0' || num2[0] == '0')
        {
            return "0";
        }
        int n = num1.size() + num2.size();
        vector<int> a(n, -1);
        string s;
        for (int i = 0; i < num1.size(); i++)
        {
            for (int j = 0; j < num2.size(); j++)
            {
                a[i + j] += ((num1[i] - '0') * (num2[j] - '0') + 1);
            }
        }
        for (int i = n; i >= 1; i--)
        {
            if (a[i] != -1)
            {
                a[i - 1] += a[i] / 10;
                a[i] %= 10;
            }
        }

        for (int i = 0; i < n; i++)
        {
            if (a[i] != -1)
            {
                s += to_string(a[i]);
            }
            else
            {
                break;
            }
        }
        return s;
    }
};

int main()
{
    Solution().multiply("3","2");
    return 0;
}
