#pragma once

#ifdef __cplusplus 
extern "C"
{
#endif
	int Add_c(int a, int b);
#ifdef __cplusplus 
}
#endif

//也可以这样:
//extern "C" int Add_c(int a, int b);