#pragma once
#include<iostream>
using namespace std;

template<class K>
struct BSTreeNode
{
	BSTreeNode(const K& key)
		:_key(key)
		,_left(nullptr)
		,_right(nullptr)
	{
		;
	}

	K _key;
	BSTreeNode<K>* _left;
	BSTreeNode<K>* _right;
};


template<class K>
class BSTree
{
public:
	typedef BSTreeNode<K> node;

	BSTree() = default;

	
	BSTree(const BSTree<K>& t)
	{
		_root = CreatBSTree(t._root);
	}

	BSTree<K> operator=(BSTree<K> t)
	{
		std::swap(_root, t._root);
		return *this;
	}

	~BSTree()
	{
		Destroy(_root);
		_root = nullptr;
	}

	bool insert(const K& key)
	{
		if (_root == nullptr)
		{
			_root = new node(key);
		}
		node* parent = nullptr;
		node* cur = _root;
		while (cur)
		{
			if (key > cur->_key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (key < cur->_key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}

		if (parent->_key > key)
		{
			parent->_left = new node(key);
		}
		else
		{
			parent->_right = new node(key);
		}

		return true;
	}


	bool erase(const K& key)
	{
		if (_root == nullptr)
		{
			return false;
		}
		node* parent = nullptr;
		node* cur = _root;

		while (cur)
		{
			if (cur->_key > key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (cur->_key < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				if (cur->_left == nullptr)
				{
					if (cur == _root)
					{
						_root = cur->_right;
						delete cur;
						return true;
					}

					if (parent->_left == cur)
					{
						parent->_left = cur->_right;
					}
					else
					{
						parent->_right = cur->_right;
					}
					delete cur;
					return true;
				}
				else if (cur->_right == nullptr)
				{
					if (cur == _root)
					{
						_root = cur->_left;
					}

					if (parent->_left == cur)
					{
						parent->_left = cur->_left;
					}
					else
					{
						parent->_right = cur->_left;
					}
					delete cur;
					return true;
				}
				else
				{
					node* parent = cur;
					node* left_max = cur->_left;
					while (left_max->_right)
					{
						parent = left_max;
						left_max = left_max->_right;
					}

					cur->_key = left_max->_key;

					if (parent->_left == left_max)
					{
						parent->_left = left_max->_left;
					}
					else
					{
						parent->_right = left_max->_left;
					}

					delete left_max;
					return true;
				}
			}
		}
		return false;
	}

	void inorder()
	{
		_inorder(_root);
		cout << endl;
	}

	bool find(const K& key)
	{
		node* cur = _root;
		while (cur)
		{
			if (cur->_key == key)
			{
				return true;
			}
			else if (cur->_key > key)
			{
				cur = cur->_left;
			}
			else
			{
				cur = cur->_right;
			}
		}
		return false;
	}

	bool insertR(const K& key)
	{
		return _insertR(_root, key);
	}

	bool findR(const K& key)
	{
		_findR(_root, key);
	}

	bool eraseR(const K& key)
	{
		return _eraseR(_root, key);
	}
private:
	void Destroy(node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		Destroy(root->_left);
		Destroy(root->_right);
		delete root;
	}

	node* CreatBSTree(node* r)
	{
		if ( r== nullptr)
		{
			return nullptr;
		}
		node* root = new node(r->_key);
		root->_left = CreatBSTree(r->_left);
		root->_right = CreatBSTree(r->_right);
		return root;
	}

	bool _eraseR(node*& root, const K& key)
	{
		if (root == nullptr)
		{
			return false;
		}

		while (root)
		{
			if (root->_key == key)
			{
				if (root->_left == nullptr)
				{
					root = root->_right;
					return true;
				}
				else if (root->_right == nullptr)
				{
					root = root->_left;
					return true;
				}
				else
				{
					node* left_max = root->_left;
					while (left_max->_right)
					{
						left_max = left_max->_right;
					}
					root->_key = left_max->_key;
					
					_eraseR(root->_left, left_max->_key);
				}
			}
			else if (root->_key > key)
			{
				return _eraseR(root->_left, key);
			}
			else
			{
				return _eraseR(root->_right, key);
			}
		}
	}

	bool _findR(node*& root, const K& key)
	{
		if (root == nullptr)
		{
			return false;
		}
		if (root->_key == key)
		{
			return true;
		}
		else if (root->_key < key)
		{
			return _findR(root->_right, key);
		}
		else
		{
			return _findR(root->_left, key);
		}
	}

	bool _insertR(node*& root, const K& key)
	{
		if (root == nullptr)
		{
			root = new node(key);
			return true;
		}
		if (root->_key > key)
		{
			return _insertR(root->_left, key);
		}
		else if (root->_key < key)
		{
			return _insertR(root->_right, key);
		}
		else
		{
			return false;
		}
	}
	void _inorder(node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		_inorder(root->_left);
		cout << root->_key << " ";
		_inorder(root->_right);
	}
private:
	node* _root = nullptr;
};
