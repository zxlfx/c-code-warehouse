#define _CRT_SECURE_NO_WARNINGS 1
#include"BinarySearchTree.h"

//int main()
//{
//	BSTree<int> bst;
//	bst.insert(4);
//	bst.insert(40);
//	bst.insert(1);
//	bst.insert(4);
//	bst.insert(9);
//	bst.insert(9);
//	bst.insert(4);
//	bst.insert(7);
//	bst.insert(3);
//	bst.insert(4);
//	bst.insert(4);
//	bst.insert(10);
//	bst.inorder();
//
//	bst.erase(1);
//	bst.erase(3);
//	bst.erase(4);
//	bst.erase(7);
//	bst.erase(9);
//	bst.erase(10);
//	//bst.erase(4);
//	bst.erase(40);
//
//	
//	bst.inorder();
//	//cout << bst.find(1) << endl;
//
//	return 0;
//}


int main()
{
	BSTree<int> t;
	t.insertR(1);
	t.insertR(9);
	t.insertR(2);
	t.insertR(4);
	t.insertR(7);
	t.insertR(10);
	t.insertR(3);
	t.insertR(6);
	t.insertR(5);

	t.inorder();

	BSTree<int> copyt(t);
	BSTree<int> t1;
	t1 = t;

	/*t.eraseR(1);
	t.eraseR(2);
	t.eraseR(3);
	t.eraseR(4);
	t.eraseR(6);
	t.eraseR(5);
	t.eraseR(7);
	t.eraseR(9);
	t.eraseR(10);*/

	t1.inorder();

	return 0;
}