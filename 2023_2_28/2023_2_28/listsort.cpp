#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

struct ListNode
{
    int _val;
    ListNode* _next;
public:
    ListNode(int val = 0) :_val(val), _next(nullptr)
    {}
};

class Solution {
public:
    ListNode* oddEvenList(ListNode* head)
    {
        ListNode* cur = head;
        ListNode* oldList = nullptr;
        ListNode* oldTail = nullptr;

        ListNode* evenList = nullptr;
        ListNode* evenTail = nullptr;
        if (!cur && !cur->_next && !cur->_next->_next)
        {
            return head;
        }

        int flag = 1;
        while (cur)
        {
            if (flag == 1)
            {
                if (oldList == nullptr)
                {
                    oldList = oldTail = cur;
                }
                else
                {
                    oldTail->_next = cur;
                    oldTail = cur;
                }
                flag = 0;
            }
            else
            {
                if (evenList == nullptr)
                {
                    evenList = evenTail = cur;
                }
                else
                {
                    evenTail->_next = cur;
                    evenTail = cur;
                }
                flag = 1;
            }

            cur = cur->_next;
        }

        evenTail->_next = nullptr;
        oldTail->_next = evenList;
        return oldList;
    }
};

int main()
{
    ListNode* n1 = new ListNode(4);
    ListNode* n2 = new ListNode(1);
       
    n1->_next = n2;
    ListNode* ret  =Solution().oddEvenList(n1);
    return 0;
}