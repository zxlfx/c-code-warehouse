#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
#include<functional>
using namespace std;

void test1()
{
	vector<int> v = { 3,2,1 };
	sort(v.begin(), v.end());
	do
	{
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	} while (next_permutation(v.begin(), v.end()));
}

void test2()
{
	vector<int> v = { 3,2,1 };
	sort(v.begin(), v.end(),greater<int>());
	do
	{
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	} while (prev_permutation(v.begin(), v.end()));
}
int main()
{
	test2();
	return 0;
}