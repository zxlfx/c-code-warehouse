#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;

class Date
{
public:
	Date(int year, int month, int day)
		:_year(year)
		,_month(month)
		,_day(day)
	{}
	
	void Print()
	{
		cout << "year:" << _year << " " << "month:" << _month << " " << "day:" << _day << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};
int main()
{
	int a = 1;
	cout << a << endl;
	Date d = { 2022, 2, 3 };
	d.Print();

	//int b = 10;
	//int&& ap = 1;
	//int&& bp = move(b);
	//bp = 3;
	//cout << b <<":" << bp << endl;
	//string s1 = "����";
	//string s2 = "����";
	//s1 = move(s2);
	//cout << "s1:" << s1 << endl;
	//cout << "s2:" << s2 << endl;
	return 0;
}