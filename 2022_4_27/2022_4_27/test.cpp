#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
class A
{
public:
	A()
	{
		cout << "默认构造" << endl;
		_a = new int(0);
	}
	A(const A& a)
	{
		_a = a._a;
		cout << "拷贝构造" << endl;
	}
	int operator[](int x)
	{
		
		return _a[x];;
	}
private:
	int* _a;
};

A f(A a)
{
	cout << "&&&&" << endl;
	return a;
}

int main()
{
	A a1;
	a1[0] = 1;
	return 0;
}