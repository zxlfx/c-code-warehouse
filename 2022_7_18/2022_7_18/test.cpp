#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//class A
//{
//public:
//	A(int a = 10, double b = 2.5)
//	{
//		_a = a;
//		_b = b;
//	}
//private:
//	int _a;
//	double _b;
//};
//
//int main()
//{
//	A* p1 = (A*)malloc(sizeof(A));
//	A* p2 = new A;
//	return 0;
//}

//int main()
//{
//	int* p1 = new int;
//	int* p2 = new int[10];
//
//	delete p1;
//	delete[] p2;
//	return 0;
//}

//class A
//{
//public:
//	A(int a = 10, double b = 2.5)
//	{
//		_a = a;
//		_b = b;
//		cout << "A()" << a << ":" << b << endl;
//	}
//
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//private:
//	int _a;
//	double _b;
//};
//
//int main()
//{
//	/*A* p2 = (A*)operator new(sizeof(A));
//	new(p2)A(5,5.5);*/
//	A* p2 = new A(5,5.5);
//
//	/*p2->~A();
//	operator delete(p2);*/
//	delete p2;
//	return 0;
//}


//int main()
//{
//	A* a = new A;
//	a->~A();
//	return 0;
//}


//template<class T>
//void Swap(T& a, T& b)
//{
//	T temp = a;
//	a = b;
//	b = temp;
//}
//
//int main()
//{
//
//	return 0;
//}


//template<class T1,class T2> 
//void Add(const T1& left, const T2& right) 
//{
//	cout << "void Add(const T1& left, const T2& right)" << endl;
//}
//
//void Add(int& a, int& b)
//{
//	cout << "int Add(int& a, int& b)" << endl;
//}
//
//int main()
//{
//	int a = 0;
//	double b = 5.5;
//	Add(a, b);
//	return 0;
//}

//template<class T>
//class Seqlist
//{
//
//public:
//	Seqlist()
//	{
//		//构造函数
//	}
//	~Seqlist()
//	{
//		//析构函数
//	}
//private:
//	T* _a;
//	size_t _size;
//	size_t _capacity;
//};
//
//int main()
//{
//	Seqlist<int> s1;//存int的顺序表
//	Seqlist<double> s2;//存double的顺序表
//	return 0;
//}