#include"Date.h"

Date::Date(int year, int month, int day)
{
	if (year <= 0 || month <= 0 || month > 12 || day <= 0 || day > GetMonthDay(year, month))
	{
		printf("无效日期\n");
	}
	else
	{
		_month = month;
		_year = year;
		_day = day;
	}

}

ostream& operator<<(ostream& out, Date& d)
{
	out << d._year << "年" << d._month << "月" << d._day << "日" << endl;
	return out;
}

istream& operator>>(istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;
	return in;
}