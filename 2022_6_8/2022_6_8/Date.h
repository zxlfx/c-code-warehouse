#pragma once
#include<iostream>
using namespace std;

class Date
{

public:
	friend ostream& operator<<(ostream& out, Date& d);
	friend istream& operator>>(istream& in, Date& d);

	Date(int year = 2022, int month = 1, int day = 1);

	Date(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

private:
	bool IsLeapYear(int year)
	{
		if (year % 4 == 0 && year % 100 != 0
			|| year % 400 == 0)
		{
			return true;
		}
		return false;
	}

	int GetMonthDay(int year, int month)
	{
		static int a[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
		if (IsLeapYear(year))
		{
			return a[month] + 1;
		}
		return a[month];
	}

	int _month;
	int _year;
	int _day;
};

ostream& operator<<(ostream& out, Date& d);

istream& operator>>(istream& in, Date& d);