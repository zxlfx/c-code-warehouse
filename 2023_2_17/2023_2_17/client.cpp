#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:4996)
#include"util.shuai"
#include<winsock2.h>
#include<thread>
#pragma comment(lib,"ws2_32.lib")

int port = 8087;
string ip = "43.138.106.72";

class Client_Udp
{
private:
    int _sock;
    uint16_t _port;
    string _ip;
public:
    Client_Udp(int port, string ip)
        :_sock(-1)
        , _port(port)
        , _ip(ip)
    {}

    ~Client_Udp()
    {}

    void Init()
    {
        WORD wVersionRequested;
        WSADATA wsaData;
        int err;
        wVersionRequested = MAKEWORD(1, 1);
        err = WSAStartup(wVersionRequested, &wsaData);
        if (err != 0) {
            exit(1);
        }

        if (LOBYTE(wsaData.wVersion) != 1 ||
            HIBYTE(wsaData.wVersion) != 1) {
            WSACleanup();
            exit(2);
        }

        _sock = socket(AF_INET, SOCK_DGRAM, 0);
        if (_sock < 0)
        {
            logmessage(FATAL, "%s:%d", strerror(errno), errno);
            exit(SOCKET_ERR);
        }

        logmessage(DEBUG, "%s:%d", strerror(errno), errno);

    }

    static char* U2G(const char* utf8)
    {
        int len = MultiByteToWideChar(CP_UTF8, 0, utf8, -1, NULL, 0);
        wchar_t* wstr = new wchar_t[len + 1];
        memset(wstr, 0, len + 1);
        MultiByteToWideChar(CP_UTF8, 0, utf8, -1, wstr, len);
        len = WideCharToMultiByte(CP_ACP, 0, wstr, -1, NULL, 0, NULL, NULL);
        char* str = new char[len + 1];
        memset(str, 0, len + 1);
        WideCharToMultiByte(CP_ACP, 0, wstr, -1, str, len, NULL, NULL);
        if (wstr) delete[] wstr;
        return str;
    }

    static char* G2U(const char* gb2312)//gdb 转 utf_8函数, vs是默认是gdb, linux默认是utf_8
    {
        int len = MultiByteToWideChar(CP_ACP, 0, gb2312, -1, NULL, 0);
        wchar_t* wstr = new wchar_t[len + 1];
        memset(wstr, 0, len + 1);
        MultiByteToWideChar(CP_ACP, 0, gb2312, -1, wstr, len);
        len = WideCharToMultiByte(CP_UTF8, 0, wstr, -1, NULL, 0, NULL, NULL);
        char* str = new char[len + 1];
        memset(str, 0, len + 1);
        WideCharToMultiByte(CP_UTF8, 0, wstr, -1, str, len, NULL, NULL);
        if (wstr) delete[] wstr;
        return str;
    }

    void Start()
    {
        string str;

        struct sockaddr_in server;
        server.sin_family = AF_INET;
        server.sin_port = htons(_port);
        server.sin_addr.s_addr = inet_addr(_ip.c_str());
        char* buf = nullptr;
        while (1)
        {
            cout << "Please Enter# ";
            fflush(stdout);
            getline(cin, str);
            buf = G2U(str.c_str());
            sendto(_sock, buf, strlen(buf), 0, (struct sockaddr*)&server, sizeof(server));
        }
    }

    int GetSock()
    {
        return _sock;
    }
};



void RecvFrom(int sock)
{
    char buf[1024] = { 0 };
    struct sockaddr_in temp;
    int len = sizeof(temp);
    while (1)
    {
        int s = recvfrom(sock, buf, sizeof(buf) - 1, 0, (struct sockaddr*)&temp, &len);
        cout << s << endl;
        Sleep(10000);
        printf("server->%s\n", Client_Udp::U2G(buf));
    }
}

int main()
{
    Client_Udp cli(port, ip);
    thread recv(RecvFrom, cli.GetSock());

    cli.Init();
    cli.Start();

    recv.join();
    WSACleanup();
}