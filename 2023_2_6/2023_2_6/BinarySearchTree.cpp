#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

template<class K>
struct BTNode
{
	BTNode* _lchild;
	BTNode* _rchild;
	K _key;

	BTNode(const K& x)
		:_lchild(nullptr)
		,_rchild(nullptr)
		,_key(x)
	{}
};

template<class K>
class BinarySearchTree
{
	typedef BTNode<K> Node;
private:
	void Destroy(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}

		Destroy(root->_lchild);
		Destroy(root->_rchild);
		delete root;
	}
	Node* CopyTree(Node* root)
	{
		if (root == nullptr)
		{
			return nullptr;
		}

		Node* newnode = new Node(root->_key);
		newnode->_lchild = CopyTree(root->_lchild);
		newnode->_rchild = CopyTree(root->_rchild);
		return newnode;
	}

public:
	~BinarySearchTree()
	{
		Destroy(_root);
		_root = nullptr;
	}
	BinarySearchTree() = default;
	BinarySearchTree(const BinarySearchTree& bt)
	{
		_root = CopyTree(bt._root);
	}

	bool Insert(const K& x)
	{
		if (_root == nullptr)
		{
			_root = new Node(x);
			return true;
		}

		Node* cur = _root;
		Node* partent = nullptr;

		while (cur)
		{
			if (cur->_key > x)
			{
				partent = cur;
				cur = cur->_lchild;
			}
			else if (cur->_key < x)
			{
				partent = cur;
				cur = cur->_rchild;
			}
			else
			{
				return false;
			}
		}

		Node* newnode = new Node(x);
		if (partent->_key > x)
		{
			partent->_lchild = newnode;
		}
		else
		{
			partent->_rchild = newnode;
		}
		return true;
	}

	void Inorder()
	{
		_Inorder(_root);
		cout << endl;
	}

	bool Find(const K& x)
	{
		if (_root == nullptr)
		{
			return false;
		}

		Node* cur = _root;
		while (cur)
		{
			if (cur->_key < x)
			{
				cur = cur->_rchild;
			}
			else if (cur->_key > x)
			{
				cur = cur->_lchild;
			}
			else
			{
				return true;
			}
		}
		return false;
	}


	bool Erase(const K& key)
	{
		Node* cur = _root;
		Node* partent = nullptr;
		while (cur)
		{
			if (cur->_key < key)
			{
				partent = cur;
				cur = cur->_rchild;
			}
			else if (cur->_key > key)
			{
				partent = cur;
				cur = cur->_lchild;
			}
			else
			{
				if (cur->_lchild == nullptr)
				{
					if (partent == nullptr)
					{
						_root = cur->_rchild;
					}
					else
					{
						if (partent->_lchild == cur)
						{
							partent->_lchild = cur->_rchild;
						}
						else
						{
							partent->_rchild = cur->_rchild;
						}
					}

					delete cur;
					cur = nullptr;
				}
				else if (cur->_rchild == nullptr)
				{
					if (partent == nullptr)
					{
						_root = cur->_lchild;
					}
					else
					{
						if (partent->_lchild == cur)
						{
							partent->_lchild = cur->_lchild;
						}
						else
						{
							partent->_rchild = cur->_lchild;
						}
					}

					delete cur;
					cur = nullptr;
				}
				else//�滻��ɾ��
				{
					Node* partent = cur;
					Node* minRight = cur->_rchild;
					while (minRight->_lchild)
					{
						partent = minRight;
						minRight = minRight->_lchild;
					}

					cur->_key = minRight->_key;//�滻ɾ��cur�ڵ�
					if (partent->_lchild == minRight)
					{
						partent->_lchild = minRight->_rchild;
					}
					else
					{
						partent->_rchild = minRight->_rchild;
					}

					delete minRight;
					minRight = nullptr;
				}
				return true;
			}
		}
		return false;
	}
private:
	void _Inorder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}

		_Inorder(root->_lchild);
		cout << root->_key << " ";
		_Inorder(root->_rchild);
	}

private:
	Node* _root = nullptr;
};

void test1()
{
	int a[] = { 8,3,1,10,6,4,7,14,13 };
	BinarySearchTree<int> bst;
	for (auto& e : a)
	{
		bst.Insert(e);
	}

	bst.Inorder();
	cout << endl;

	for (auto& e : a)
	{
		bst.Erase(e);
	}

	cout << "------------------" << endl;
	bst.Inorder();
	cout << endl;
}

void test2()
{
	BinarySearchTree<int> b1;
	b1.Insert(1);
	b1.Insert(3);
	b1.Insert(2);
	b1.Inorder();

	BinarySearchTree<int> b2(b1);
	b2.Inorder();

}

int main()
{
	test2();
	return 0;
}