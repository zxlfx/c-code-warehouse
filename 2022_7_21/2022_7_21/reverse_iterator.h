#pragma once
//第一种实现方法：带三个参数的，实现起来简单方便，可以适配任何的容器，包括vector、string。
//template<class Iterator, class Ref, class Ptr>
//struct Reverse_iterator
//{
//	typedef Reverse_iterator<Iterator, Ref, Ptr> Self;
//
//	Reverse_iterator(Iterator it)
//	{
//		_it = it;
//	}
//
//	Ref operator*()
//	{
//		Iterator temp = _it;
//		return *(--temp);
//	}
//
//	Ptr operator->()
//	{
//		Iterator temp = _it;
//		return &(*(--temp));
//	}
//
//	Self& operator--()
//	{
//		++_it;
//		return *this;
//	}
//
//	Self operator--(int)
//	{
//		_it++;
//		return *this;
//	}
//
//	Self& operator++()
//	{
//		_it--;
//		return *this;
//	}
//
//	Self operator++(int)
//	{
//		Iterator temp = _it;
//		_it--;
//		return temp;
//	}
//
//	bool operator!=(const Self& s)
//	{
//		return _it != s._it;
//	}
//
//	Iterator _it;
//};



//第二种实现方法：只用一个参数，Ref和Ptr从正向迭代器中萃取出来。
// 缺点：不适配与vector、string这种原生指针做迭代器的。
//template<class Iterator>
//struct Reverse_iterator
//{
//	typedef Reverse_iterator<Iterator> Self;
//	typedef typename Iterator::reference Ref;
//	typedef typename Iterator::pointer Ptr;
//
//	Reverse_iterator(Iterator it)
//	{
//		_it = it;
//	}
//
//	Ref operator*()
//	{
//		Iterator temp = _it;
//		return *(--temp);
//	}
//
//	Ptr operator->()
//	{
//		Iterator temp = _it;
//		return &(*(--temp));
//	}
//
//	Self& operator--()
//	{
//		++_it;
//		return *this;
//	}
//
//	Self operator--(int)
//	{
//		_it++;
//		return *this;
//	}
//
//	Self& operator++()
//	{
//		_it--;
//		return *this;
//	}
//
//	Self operator++(int)
//	{
//		Iterator temp = _it;
//		_it--;
//		return temp;
//	}
//
//	bool operator!=(const Self& s)
//	{
//		return _it != s._it;
//	}
//
//	Iterator _it;
//};


//第三种方法：使用模板的特化处理原生指针不适配的问题。
template<class Iterator>
struct Iterator_traits
{
	typedef typename Iterator::reference reference;
	typedef typename Iterator::pointer pointer;
};

template<class T>
struct Iterator_traits<T*>
{
	typedef T* reference;
	typedef T& pointer;
};

template<class T>
struct Iterator_traits<const T*>
{
	typedef const T* reference;
	typedef const T& pointer;
};


template<class Iterator>
struct Reverse_iterator
{
	typedef Reverse_iterator<Iterator> Self;
	typedef typename Iterator_traits<Iterator>::reference Ref;
	typedef typename Iterator_traits<Iterator>::pointer Ptr;

	Reverse_iterator(Iterator it)
	{
		_it = it;
	}

	Ref operator*()
	{
		Iterator temp = _it;
		return *(--temp);
	}

	Ptr operator->()
	{
		Iterator temp = _it;
		return &(*(--temp));
	}

	Self& operator--()
	{
		++_it;
		return *this;
	}

	Self operator--(int)
	{
		_it++;
		return *this;
	}

	Self& operator++()
	{
		_it--;
		return *this;
	}

	Self operator++(int)
	{
		Iterator temp = _it;
		_it--;
		return temp;
	}

	bool operator!=(const Self& s)
	{
		return _it != s._it;
	}

	Iterator _it;
};