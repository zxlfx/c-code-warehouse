#pragma once
#include"reverse_iterator.h"
#include<iostream>
#include<cassert>
using namespace std;

namespace ds
{
	template<class T>
	struct __list_node
	{
		__list_node(const T& x = T())
			:val(x)
			,prev(nullptr)
			,next(nullptr)
		{
			;
		}

		T val;
		__list_node<T>* prev;
		__list_node<T>* next;
	};

	template<class T,class Ref,class Ptr>
	struct Iterator
	{
	public:
		typedef __list_node<T> node;
		typedef Iterator<T, Ref, Ptr> sef;
		typedef Ref reference;
		typedef Ptr pointer;


		Iterator(node* node = nullptr)
		{
			cur = node;
		}

		Ref operator*()
		{
			return cur->val;
		}

		Ptr operator->()
		{
			return &cur->val;
		}

		sef operator++()
		{
			cur = cur->next;
			return cur;
		}

		sef operator--()
		{
			cur = cur->prev;
			return cur;
		}

		sef operator++(int)
		{
			sef temp = *this;
			cur = cur->next;
			return temp;
		}

		sef operator--(int)
		{
			sef temp = *this;
			cur = cur->prev;
			return temp;
		}

		bool operator!=(sef it)
		{
			return cur != it.cur;
		}

		bool operator==(sef it)
		{
			return cur == it.cur;
		}

		node* cur;
	};

	//template<class T>
	//struct cosnt_Iterator
	//{
	//public:
	//	typedef __list_node<T> node;
	//	typedef const_Iterator<T> sef;
	//	const_Iterator(node* node = nullptr)
	//	{
	//		cur = node;
	//	}

	//	const T& operator*()
	//	{
	//		return cur->val;
	//	}

	//	const T* operator->()
	//	{
	//		return &cur->val;
	//	}

	//	sef& operator++()
	//	{
	//		cur = cur->next;
	//		return cur;
	//	}

	//	sef& operator--()
	//	{
	//		cur = cur->prev;
	//		return cur;
	//	}

	//	sef operator++(int)
	//	{
	//		sef temp = *this;
	//		cur = cur->next;
	//		return temp;
	//	}

	//	sef operator--(int)
	//	{
	//		sef temp = *this;
	//		cur = cur->prev;
	//		return temp;
	//	}

	//	bool operator!=(sef it)
	//	{
	//		return cur != it.cur;
	//	}

	//	bool operator==(sef it)
	//	{
	//		return cur == it.cur;
	//	}

	//	node* cur;
	//};



	template<class T>
	class list
	{
	public:
		typedef __list_node<T> node;

		typedef Iterator<T, T&, T*> iterator;
		typedef Iterator<T, const T&, const T*> const_iterator;

		/*typedef Reverse_iterator<iterator, T&, T*> reverse_iterator;
		typedef Reverse_iterator<const_iterator, const T&, const T*> const_reverse_iterator;*/

		typedef Reverse_iterator<iterator> reverse_iterator;
		typedef Reverse_iterator<const_iterator> const_reverse_iterator;

		void empty_init()
		{
			_head = new node();
			_head->prev = _head;
			_head->next = _head;
		}

		list()
		{
			_head = new node();
			_head->prev = _head;
			_head->next = _head;
		}

		template<class InputIteartor>
		list(InputIteartor first, InputIteartor last)
		{
			_head = new node();
			_head->prev = _head;
			_head->next = _head;
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}

		void push_back(const T& x)
		{
			/*node* newnode = new node(x);
			node* tail = _head->prev;

			tail->next = newnode;
			newnode->prev = tail;
			newnode->next = _head;
			_head->prev = newnode;*/

			insert(end(), x);
		}

		list(const list<T>& t)//传统写法
		{
			empty_init();
			for (auto& a : t)
			{
				push_back(a);
			}
		}

		//list(const list<T>& t)//现代写法
		//{
		//	list<T> temp(t.begin(), t.end());
		//	swap(temp);
		//}

		list<T>& operator=(const list<T>& t)//传统写法
		{
			if (this != &t)
			{
				clear();
				for (auto& e : t)
				{
					push_back(e);
				}
			}
			return *this;
		}

		//list<T>& operator=(list<T> t)//现代写法
		//{
		//	swap(t);
		//	return *this;
		//}

		void swap(list<T>& t)
		{
			std::swap(_head, t._head);
		}

		void push_front(const T& x)
		{
			insert(begin(), x);
		}

		iterator insert(iterator pos ,const T& x)
		{
			node* newnode = new node(x);
			node* cur = pos.cur;
			node* prev = cur->prev;

			prev->next = newnode;
			newnode->prev = prev;
			newnode->next = cur;
			cur->prev = newnode;
			return pos;
		}


		void pop_back()
		{
			erase(--end());
		}

		void pop_front()
		{
			erase(begin());
		}

		void clear()
		{
			iterator it = begin();
			while (it != begin())
			{
				it = erase(it);
			}
		}

		~list()
		{
			clear();
			delete _head;
			_head = nullptr;
		}

		iterator erase(iterator pos)
		{
			assert(pos.cur != _head);
			node* next = pos.cur->next;
			node* prev = pos.cur->prev;

			next->prev = prev;
			prev->next = next;
			delete pos.cur;
			return next;
		}

		iterator begin()
		{
			return iterator(_head->next);
		}

		iterator end()
		{
			return iterator(_head);
		}

		const_iterator begin()const
		{
			return const_iterator(_head->next);
		}

		const_iterator end()const 
		{
			return const_iterator(_head);
		}

		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}

		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}

		const_reverse_iterator rbegin()const
		{
			return const_reverse_iterator(end());
		}

		const_reverse_iterator rend()const
		{
			return const_reverse_iterator(begin());
		}
	private:
		node* _head;
	};


	void test_list1()
	{
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		lt.push_back(5);


		/*lt.pop_front();
		lt.pop_front();
		lt.pop_back();*/

		//const list<int> clt(lt.begin(), lt.end());

		list<int>::reverse_iterator it = lt.rbegin();
		while (it != lt.rend())
		{
			//(*it) += 1;
			cout << *it << " ";
			++it;
		}
		cout << endl;
	}
}
