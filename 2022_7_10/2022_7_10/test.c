#define _CRT_SECURE_NO_WARNINGS 1


#include<iostream>
using namespace std;

//int main()
//{
//	const char ch = 0;
//	const char* p1 = &ch;
//	char* p2 = p1;
//	return 0;
//}

string& insert(size_t pos, const char* str)
{
	assert(pos <= _size);
	size_t len = strlen(str);
	if (_size + len > _capacity)
	{
		reserve(_size + len);
	}

	size_t end = _size + len;
	while (end - len >= pos)
	{
		_str[end] = str[end - len];
		end--;
	}
	strncpy(_str + pos, str, len);
	_size += len;
	return *this;
}