#include<iostream>
using namespace std;

class Solution {
public:
    /**
     * 进制转换
     * @param M int整型 给定整数
     * @param N int整型 转换到的进制
     * @return string字符串
     */
    string solve(int M, int N)
    {
        string s;
        while (M / N)
        {
            switch (M)
            {
            case 10:
                s += 'A';
                break;
            case 11:
                s += 'B';
                break;
            case 12:
                s += 'C';
                break;
            case 13:
                s += 'D';
                break;
            case 14:
                s += 'E';
                break;
            case 15:
                s += 'F';
                break;
            default:
                s += (M % N + '0');
                break;
            }
            M /= N;
        }
        switch (M)
        {
        case 10:
            s += 'A';
            break;
        case 11:
            s += 'B';
            break;
        case 12:
            s += 'C';
            break;
        case 13:
            s += 'D';
            break;
        case 14:
            s += 'E';
            break;
        case 15:
            s += 'F';
            break;
        default:
            s += (M % N + '0');
            break;
        }
        reverse(s.begin(), s.end());
        return s;
    }
};

int main()
{
    Solution().solve(23, 12);
	return 0;
}