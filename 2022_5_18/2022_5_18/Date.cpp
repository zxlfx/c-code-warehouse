#include<iostream>
using namespace std;

class Date
{
public:
	Date(int year,int month,int day)
		:_year(year)
		,_month(month)
		,_day(day)
	{
		if (_year <= 0 || _month > 12 || _month <= 0 || _day <= 0 || _day > GetMonthDay(_year, _day))
		{
			cout << "���ڲ�����" << endl;
		}
	}

	Date& operator+=(int day)
	{
		_day += day;
		while (_day > GetMonthDay(_year, _month))
		{
			_day -= GetMonthDay(_year, _month);
			_month += 1;
			if (_month == 13)
			{
				_year++;
				_month = 1;
			}
		}
		return *this;
	}

	Date operator+(int day)
	{
		Date temp(*this);
		temp += day;
		return temp;
	}

	Date operator-(int day)
	{
		_day -= day;
		while (_day <= 0)
		{
			_month--;
			if (_month == 0)
			{
				--_year;
				_month == 12;
			}
			_day += GetMonthDay(_year, _day);
		}
	}

private:
	int GetMonthDay(int year, int month);
	int _year;
	int _month;
	int _day;
};

int Date::GetMonthDay(int year, int month)
{
	static int a[13] = { 0,31,28,31,30,31,30,31,31,30,31,30 ,31 };
	if (month==2 && year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
	{
		return a[month] + 1;
	}
	return a[month];
}
