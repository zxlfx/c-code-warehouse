#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Book_base
{
	char name[20];//书名
	char author[15];//作者
	double price;//定价
	char press[20];//出版社
	int pages;//页数
	int nums;//存量
}book;

typedef struct Book_List
{
	book* a;
	int size;//当前已有书籍数目
	int capacity;//当期书单容量
}Book_List;




void LoadData(Book_List* bs);
void SaveData(Book_List* bs);
void Init(Book_List* bs);
void AddBook(Book_List* bs);
void CheckCacity(Book_List* bs);
void ShowBookList(Book_List* bs);
void ModifyBook(Book_List* bs);
void DeleteBook(Book_List* bs);








