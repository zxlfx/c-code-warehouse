#pragma once
#include"LMS_BS.h"
#include<time.h>
#include<stdbool.h>
typedef struct Date
{
	int year;
	int month;
	int day;
}Date;
typedef struct Borrow_Message
{
	book b;//借阅的书籍
	char name[20];//借阅人的名字
	long long id;//借阅人的学号
	Date Start_d;//借阅日期
	Date End_d;//还书日期
	int n;//编号
}Borrow_Message;


typedef struct Borrow_List
{
	Borrow_Message* a;
	int size;
	int capacity;
}Borrow_List;


void Init_bt(Borrow_List* bt);
void BorrowBook(Borrow_List* bt, Book_List* bs);
void BorrowInfo(Borrow_List* bt);
void DeleteInfo(Borrow_List* bt);

void CheckCapacity_B(Borrow_List* bt);
void LoadData_B(Borrow_List* bt);
void SaveData_B(Borrow_List* bt);






