#include"LMS_BS.h"

void LoadData(Book_List* bs)
{
	FILE* pr = fopen("data.txt", "r");
	if (pr == NULL)
	{
		printf("fopen fail!\n");
		exit(-1);
	}
	while (~fscanf(pr, "%s %s %lf %s %d %d", bs->a[bs->size].name, bs->a[bs->size].author, &(bs->a[bs->size].price), bs->a[bs->size].press, &(bs->a[bs->size].pages), &(bs->a[bs->size].nums)))
	{
		bs->size++;
		CheckCacity(bs);
	}
	fclose(pr);
	pr = NULL;
}


void SaveData(Book_List* bs)
{

	FILE* pw = fopen("data.txt", "w");
	if (pw == NULL)
	{
		printf("fopen fail!\n");
		exit(-1);
	}
	int n = bs->size;
	for (int i = 0; i < n; i++)
	{
		fprintf(pw, "%-20s %-15s %-10.2lf %-20s %-10d %-10d\n", bs->a[i].name, bs->a[i].author, bs->a[i].price, bs->a[i].press, bs->a[i].pages, bs->a[i].nums);
	}
}

void Init(Book_List* bs)
{
	bs->a = (book*)malloc(sizeof(book) * 5);
	bs->size = 0;
	bs->capacity = 5;
}


void CheckCacity(Book_List* bs)
{
	if (bs->size == bs->capacity)
	{
		int newcapacity = bs->capacity * 2;
		bs->a = (book*)realloc(bs->a, sizeof(book) * newcapacity);
		bs->capacity = newcapacity;
	}
}

void AddBook(Book_List* bs)
{
	CheckCacity(bs);
	book b;
	printf("请输入书名:>");
	scanf("%s", b.name);
	printf("请输入作者:>");
	scanf("%s", b.author);
	printf("请输入定价:>");
	scanf("%lf", &b.price);
	printf("请输入出版社:>");
	scanf("%s", b.press);
	printf("请输入页数:>");
	scanf("%d", &b.pages);
	printf("请输入数量:>");
	scanf("%d", &b.nums);

	bs->a[bs->size] = b;
	bs->size++;
}

void ShowBookList(Book_List* bs)
{
	printf("****************************************图书信息****************************************\n");
	printf("%-20s %-15s %-10s %-20s %-10s %-10s\n", "书名", "作者", "定价", "出版社", "页数", "库存");
	int n = bs->size;
	for (int i = 0; i < n; i++)
	{
		printf("%-20s %-15s %-10.2lf %-20s %-10d %-10d\n", bs->a[i].name, bs->a[i].author, bs->a[i].price, bs->a[i].press, bs->a[i].pages, bs->a[i].nums);
	}
}

void ModifyBook(Book_List* bs)
{
	printf("请输入要被修改的书名:>");
	char s[20];
	int n = 0;
	int flag = 0;
	scanf("%s", s);
	for (int i = 0; i < bs->size; i++)
	{
		if (strcmp(s, bs->a[i].name) == 0)
		{
			printf("%-20s %-15s %-10.2lf %-20s %-10d %-10d\n", bs->a[i].name, bs->a[i].author, bs->a[i].price, bs->a[i].press, bs->a[i].pages, bs->a[i].nums);
			printf("请输入修改后的书名:>");
			scanf("%s", &bs->a[i].name);
			printf("作者:>");
			scanf("%s", &bs->a[i].author);
			printf("价格:>");
			scanf("%lf", &bs->a[i].price);
			printf("出版社:>");
			scanf("%s", &bs->a[i].press);
			printf("页数:>");
			scanf("%d", &bs->a[i].pages);
			printf("数量:>");
			scanf("%d", &bs->a[i].nums);
			flag = 1;
		}
	}
	if (flag == 0)
	{
		printf("书单中没有该书籍\n");
	}
}


void DeleteBook(Book_List* bs)
{
f:
	printf("请选择:>1.按书名删除 2.按作者删除\n");
	int input = 0;
	int flag = 0;
	scanf("%d", &input);
	if (input == 1)
	{
		char s[20];
		char ch = 0;
		printf("请输入要删除的书名:>");
		scanf("%s", s);

		for (int i = 0; i < bs->size; i++)
		{
			if (strcmp(s, bs->a[i].name) == 0)
			{
				printf("%-20s %-15s %-10s %-20s %-10s %-10s\n", "书名", "作者", "定价", "出版社", "页数", "库存");
				printf("%-20s %-15s %-10.2lf %-20s %-10d %-10d\n", bs->a[i].name, bs->a[i].author, bs->a[i].price, bs->a[i].press, bs->a[i].pages, bs->a[i].nums);
				printf("是否确定删除Y/N\n");
				getchar();
				scanf("%c", &ch);
				if (ch == 'y' || ch == 'Y')
				{
					int j = i;
					while (j < bs->size)
					{
						bs->a[j] = bs->a[j + 1];
						j++;
					}
					bs->size--;
				}
				printf("删除成功！\n");
				return;
				flag = 1;
			}
		}
		if (flag == 0)
		{
			printf("该书名不存在!!\n");
		}
	}
	else if (input == 2)
	{
		printf("请输入作者:>");
		char s[20];
		char ch = 0;
		int c = 1;
		int b = 0;
		int i = 0;
		int q = 0;
		scanf("%s", s);
		for (i = 0; i < bs->size; i++)
		{
			if (strcmp(s, bs->a[i].author) == 0)
			{
				if (c == 1)
				{
					printf("%-20s %-15s %-10s %-20s %-10s %-10s\n", "书名", "作者", "定价", "出版社", "页数", "库存");
					c++;
					q = i;
				}
				printf("%-20s %-15s %-10.2lf %-20s %-10d %-10d\n", bs->a[i].name, bs->a[i].author, bs->a[i].price, bs->a[i].press, bs->a[i].pages, bs->a[i].nums);
				b++;

				/*printf("是否确定删除Y/N:>");
				getchar();
				scanf("%c", &ch);
				if (ch == 'y' || ch == 'Y')
				{
					int j = i;
					while (j < bs->size)
					{
						bs->a[j] = bs->a[j + 1];
						j++;
					}
					bs->size--;
				}
				printf("删除成功！\n");
				return;
				flag = 1;*/
			}
		}

		if (b == 1)
		{
			printf("是否确定删除Y/N:>");
			getchar();
			scanf("%c", &ch);
			if (ch == 'y' || ch == 'Y')
			{
				int j = q;
				while (j < bs->size)
				{
					bs->a[j] = bs->a[j + 1];
					j++;
				}
				bs->size--;
				printf("删除成功！\n");
			}
			return;
			flag = 1;
		}
		else
		{
			int n = 0;
			char c = 0;
			printf("该作者的书籍不唯一，是选择全部删除吗？Y/N\n");
			getchar();
			scanf("%c", &c);
			if (c == 'Y' || c == 'y')
			{
				for (int i = 0; i < bs->size; i++)
				{
					if (strcmp(s, bs->a[i].author) == 0)
					{
						int j = i;
						while (j < bs->size)
						{
							bs->a[j] = bs->a[j + 1];
							j++;
						}
						bs->size--;
					}
				}
				printf("删除成功！\n");
				return;
				
			}
			return;
		}
		printf("该作者不存在!!\n");
	}
	else
	{
		printf("没有该选项,请重新输入!!");
		goto f;
	}
}


