#include"LMS_BT.h"

void CheckCapacity_B(Borrow_List* bt)
{
	if (bt->size == bt->capacity)
	{
		int newcapacity = 2 * bt->capacity;
		bt->a = (Borrow_Message*)realloc(bt->a, sizeof(Borrow_Message) * newcapacity);
		bt->capacity = newcapacity;
	}
}

void LoadData_B(Borrow_List* bt)
{
	FILE* pr = fopen("Borrowdata.txt", "r");
	if (pr == NULL)
	{
		printf("fopen fail!\n");
		exit(-1);
	}
	while (~fscanf(pr, "%s %s %lld %d年%d月%d日 %d年%d月%d日  %d\n", bt->a[bt->size].b.name, bt->a[bt->size].name, &bt->a[bt->size].id, &bt->a[bt->size].Start_d.year, &bt->a[bt->size].Start_d.month, &bt->a[bt->size].Start_d.day, &bt->a[bt->size].End_d.year, &bt->a[bt->size].End_d.month, &bt->a[bt->size].End_d.day, &bt->a[bt->size].n))
	{
		bt->size++;
		CheckCapacity_B(bt);
	}
	fclose(pr);
	pr = NULL;
}


void SaveData_B(Borrow_List* bt)
{

	FILE* pw = fopen("Borrowdata.txt", "w");
	if (pw == NULL)
	{
		printf("fopen fail!\n");
		exit(-1);
	}
	int n = bt->size;
	for (int i = 0; i < n; i++)
	{
		fprintf(pw, "%-20s %-15s %-15lld %d年%d月%d日  %10d年%d月%d日        %-8d\n", bt->a[i].b.name, bt->a[i].name, bt->a[i].id, bt->a[i].Start_d.year, bt->a[i].Start_d.month, bt->a[i].Start_d.day, bt->a[i].End_d.year, bt->a[i].End_d.month, bt->a[i].End_d.day, bt->a[i].n);
	}
}


void Init_bt(Borrow_List* bt)
{
	bt->capacity = 5;
	bt->size = 0;
	bt->a = (Borrow_Message*)malloc(sizeof(Borrow_Message) * bt->capacity);
}

bool IsLeapYear(int year)
{
	if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
	{
		return true;
	}
	return false;
}
void BorrowBook(Borrow_List* bt, Book_List* bs)
{
	long long _id = 0;
	int flag = 0;
	char _name[20];
	char s[20];
	int Dayarr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31};
	int n = 0;
	
	printf("**************************\n");
	printf("*********身份登记*********\n");
	printf("姓名:[___________________]\n");
	printf("学号:[___________________]\n");
	printf("**************************\n");

	printf("姓名:>");
	scanf("%s", _name);

	printf("学号:>");
	scanf("%lld", &_id);
	
	printf("请输入你需要的借阅的书籍名字:>");
	scanf("%s", s);
	for (int i = 0; i < bs->size; i++)
	{
		if (strcmp(s, bs->a[i].name) == 0)
		{
			if (bs->a[i].nums == 0)
			{
				printf("当前书籍库存为空，暂时无法借阅\n");
				return;
			}
			else
			{
				//借阅，登记
				bt->a[bt->size].b = bs->a[i];
				strcpy(bt->a[bt->size].name, _name);
				bt->a[bt->size].id = _id;

				

				time_t timep;
				struct tm* p;
				time(&timep);
				p = gmtime(&timep);

				bt->a[bt->size].Start_d.year = 1900 + p->tm_year;
				bt->a[bt->size].Start_d.month = 1 + p->tm_mon;
				bt->a[bt->size].Start_d.day = p->tm_mday;


				bt->a[bt->size].End_d.year = 1900 + p->tm_year;
				bt->a[bt->size].End_d.month = 1 + p->tm_mon;;
				bt->a[bt->size].End_d.day = p->tm_mday + 15;

				n = Dayarr[1 + p->tm_mon];
				if (1 + p->tm_mon == 2 && IsLeapYear(1900 + p->tm_year))
				{
					n = 29;
				}
				while (bt->a[bt->size].End_d.day > n)
				{
					(bt->a[bt->size].End_d.day) -= n;
					bt->a[bt->size].End_d.month++;
					n = Dayarr[1 + p->tm_mon];
					if (1 + p->tm_mon == 2 && IsLeapYear(1900 + p->tm_year))
					{
						n = 29;
					}
				}
				bt->a[bt->size].n = bt->size;

				bs->a[i].nums--;
				bt->size++;
				printf("借阅成功!\n");
				return;
			}//else end
			flag = 1;
		}
	}
	if (flag == 0)
	{
		printf("书库中找不到该书籍！！\n");
	}
}


void BorrowInfo(Borrow_List* bt)
{
	printf("*********************************************借阅信息*********************************************\n");
	printf("%-20s %-15s %-15s %-20s %-20s %-8s\n", "借阅书籍", "姓名", "学号", "借阅时间", "还书时间","编号");
	for (int i = 0; i < bt->size; i++)
	{
		//printf("%d年%d月%d日\n", bt->a[i].Start_d.year, bt->a[i].Start_d.month, bt->a[i].Start_d.day);
		//printf("%d年%d月%d日\n", bt->a[i].End_d.year, bt->a[i].End_d.month, bt->a[i].End_d.day);
		printf("%-20s %-15s %-15lld %d年%d月%d日  %10d年%d月%d日        %-8d\n", bt->a[i].b.name, bt->a[i].name, bt->a[i].id, bt->a[i].Start_d.year, bt->a[i].Start_d.month, bt->a[i].Start_d.day, bt->a[i].End_d.year, bt->a[i].End_d.month, bt->a[i].End_d.day,bt->a[i].n);
	}
}

void DeleteInfo(Borrow_List* bt)
{
	if (bt->size == 0)
	{
		printf("当前借阅信息为空，无法删除！！\n");
	}
	else
	{
		int n = 0;
		printf("请输入要删除的编号:>");
		scanf("%d", &n);
		for (int i = 0; i < bt->size; i++)
		{
			if (n == bt->a[i].n)
			{
				int j = i;
				while (j < bt->size)
				{
					bt->a[j] = bt->a[j + 1];
					j++;
				}
				printf("删除成功!!!\n");
				bt->size--;
				return;
			}
		}
	}
}