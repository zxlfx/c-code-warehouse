#include"LMS_BS.h"
#include"LMS_BT.h"

void menu()
{
	printf("***********1.添加系统书籍***********\n");
	printf("***********2.修改系统书籍***********\n");
	printf("***********3.删除系统书籍***********\n");
	printf("***********4.借阅系统书籍***********\n");
	printf("***********5.显示借阅信息***********\n");
	printf("***********6.删除借阅信息***********\n");
	printf("***********7.显示图书信息***********\n");
	printf("***********0.退出管理系统***********\n");
}

int main()
{
	int input = 0;
	Book_List bs;//书单
	Borrow_List bt;//借阅列表
	Init(&bs);
	Init_bt(&bt);
	LoadData(&bs);
	LoadData_B(&bt);
	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
		{
			AddBook(&bs);
			break;
		}

		case 2:
		{
			ModifyBook(&bs);
			break;
		}

		case 3:
		{
			DeleteBook(&bs);
			break;
		}

		case 4:
		{
			BorrowBook(&bt,&bs);
			break;
		}

		case 5:
		{
			BorrowInfo(&bt);
			break;
		}

		case 6:
		{
			DeleteInfo(&bt);
			break;
		}

		case 7:
		{
			ShowBookList(&bs);
			break;
		}
		case 0:
		{
			SaveData(&bs);
			SaveData_B(&bt);

			printf("已退出图书管理系统\n");
			break;
		}
		default:
		{
			printf("没有该选项，请重新输入\n");
			break;
		}

		}
	}while (input);
	return 0;
}