#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//struct A
//{
//public:
//	void Print(int x)
//	{
//		cout << "Print(int x)" << endl;
//	}
//
//public:
//	int _a = 10;
//};
//
//class B : public A
//{
//public:
//	void Print()
//	{
//		cout << "Print()" << endl;
//	}
//public:
//	int _a = 3;
//};


//int main()
//{
//	B b;
//	cout << b._a << endl;
//	cout << b.A::_a << endl;
//
//	b.Print();
//	b.A::Print(2);
//	return 0;
//}

//int main()
//{
//	A a;
//	//B* b = &a;//不行
//	B* b1 = (B*)&a;//行，但可能发生越界访问。
//	cout << b1->_b << endl;
//
//	A* pa = new B;
//	B* b2 = (B*)pa;
//	cout << b2->_b << endl;
//
//	return 0;
//}



//int main()
//{
//	//A a;
//	//a._a = 30;
//
//	//B b;
//	//b._a = 10;
//
//	//a = b;
//	//cout << a._a << endl;
//	//cout << b._a << endl;
//
//	A a;
//	//B* b = &a;//不行
//	B* b = (B*)&a;//行，但可能发生越界访问。
//	cout << b->_b << endl;
//
//	return 0;
//}

//struct A
//{
//public:
//	A(int x)
//	{
//		_a = x;
//		cout << "A(int x)" << endl;
//	}
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//public:
//	int _a = 10;
//};
//
//class B : public A
//{
//public:
//	B()
//		:A(100)
//	{
//		cout << "B()" << endl;
//	}
//
//	~B()
//	{
//		cout << "~B()" << endl;
//	}
//
//public:
//	int _b = 3;
//};
//
//int main()
//{
//	B b;
//	return 0;
//}

//class Person
//{
//public:
//	Person(const char* name = "张三")
//		: _name(name)
//	{
//		cout << "Person()" << endl;
//	}
//
//	Person(const Person& p)
//		: _name(p._name)
//	{
//		cout << "Person(const Person& p)" << endl;
//	}
//
//	Person& operator=(const Person& p)
//	{
//		if (this != &p)
//		{
//			_name = p._name;
//		}
//		cout << "Person operator=(const Person& p)" << endl;
//		return *this;
//	}
//
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//
//protected:
//	string _name; // 姓名
//};
//
//class Student : public Person
//{
//public:
//	Student(const char* name, int num)
//		: Person(name)
//		, _num(num)
//	{
//		cout << "Student()" << endl;
//	}
//
//	Student(const Student& s)
//		: Person(s)
//		, _num(s._num)
//	{
//		cout << "Student(const Student& s)" << endl;
//	}
//
//	Student& operator = (const Student& s)
//	{
//		if (this != &s)
//		{
//			Person::operator =(s);
//			_num = s._num;
//		}
//
//		cout << "Student& operator= (const Student& s)" << endl;
//		return *this;
//	}
//
//	~Student()
//	{
//		cout << "~Student()" << endl;
//	}
//
//protected:
//	int _num; //学号
//};
//
//
//int main()
//{
//	Student s1("张三",20203292);
//	Student s2("李四", 20207532);
//	Student s3 = s1;
//	s3 = s2;
//	return 0;
//}

//class Person
//{
//public:
//	Person() 
//	{
//		++_count; 
//	}
//protected:
//	string _name; // 姓名
//public:
//	static int _count; // 统计人的个数。
//};
//
//int Person::_count = 0;
//
//class Student : public Person
//{
//protected:
//	int _stuNum; // 学号
//};
//
//class PostGraduate : public Student
//{
//protected:
//	string _seminarCourse; // 研究科目
//};
//
//int main()
//{
//	Person p1;
//	Person p2;
//	Person p3;
//
//	Student s1;
//	Student s2;
//	Student s3;
//	PostGraduate s4;
//	PostGraduate s5;
//
//	cout << Person::_count << endl;
//	cout << Student::_count << endl;
//	cout << PostGraduate::_count << endl;
//
//	PostGraduate::_count = 10;
//
//	cout << Person::_count << endl;
//	cout << Student::_count << endl;
//	cout << PostGraduate::_count << endl;
//	return 0;
//}

//
//class A 
//{
//public:
//	char _a;
//};
//
//class B : virtual public A 
//{
//public:
//	int _b;
//};
//
//class C : virtual public A
//{
//public:
//	int _c;
//};
//
//class D : public B, public C 
//{
//public:
//	int _d;
//};
//
//int main()
//{
//	cout << sizeof(A) << endl;
//	cout << sizeof(B) << endl;
//	cout << sizeof(C) << endl;
//	cout << sizeof(D) << endl;
//	return 0;
//}


//class A
//{
//public:
//	char _a;
//};
//
//class B : virtual public A
//{
//public:
//	int _b;
//};
//
//class C : virtual public A
//{
//public:
//	int _c;
//};
//
//class D : public B, public C
//{
//public:
//	int _d;
//};
//
//int main()
//{
//	A a;
//	a._a = 1;
//
//	B b;
//	b._a = 2;
//	b._b = 3;
//
//	C c;
//	c._a = 4;
//	c._c = 5;
//
//	D d;
//	d._a = 6;
//	d._b = 7;
//	d._c = 8;
//	d._d = 9;
//	return 0;
//}


class A
{
private:
	int _a;
};

class B
{
private:
	A a;
	int _b;
};
int main()
{
	
	return 0;
}