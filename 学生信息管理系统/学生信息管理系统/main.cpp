#include"stduent-message-sys.h"

void menu()
{
	cout << "**************************************************" << endl;
	cout << "****   my stduent_message_management_system   ****" << endl;
	cout << "*****************    1.Add()      *****************" << endl;
	cout << "*****************    2.Delete()   *****************" << endl;
	cout << "*****************    3.Search()   *****************" << endl;
	cout << "*****************    4.Modify()   *****************" << endl;
	cout << "*****************    5.Show()     *****************" << endl;
	cout << "*****************    6.Screen     *****************" << endl;
	cout << "*****************    7.Clear     *****************" << endl;
	cout << "*****************    0.Exit       *****************" << endl;
	cout << "***************************************************" << endl;
}

int main()
{
	menu();
	System s;
	int n = 0;
	do
	{
		cout << "请选择:>";
		cin >> n;
		switch (n)
		{
		case 1:
			s.Add();
			break;
		case 2:
			s.Delete();
			break;
		case 3:
			s.Search();
			break;
		case 4:
			s.Modify();
			break;
		case 5:
			s.Show();
			break;
		case 6:
			s.Screen();
			break;
		case 7:
			system("cls");
			menu();
			break;
		case 0:
			s.Write();
			cout << "系统成功退出！！！" << endl;
			break;
		default:
			cout << "无该选项，请重新输入！！!" << endl;
			break;
		}
	} while (n);
}