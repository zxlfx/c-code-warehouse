#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<stack>
using namespace std;

//bool isValid(string s)
//{
//    stack<char> sk;
//    for (auto e : s)
//    {
//        switch (e)
//        {
//        case '(':
//        case '[':
//        case '{':
//        {
//            sk.push(e);
//        }
//        default:
//        {
//            if (sk.empty() || sk.top() != e)
//            {
//                return false;
//            }
//            sk.pop();
//        }
//        }
//    }
//    if (sk.empty())
//        return true;
//    return false;
//}


//inline int GetPriority(char ch)
//{
//    if (ch == '+' || ch == '-')
//        return 1;
//    if (ch == '*' || ch == '/')
//        return 2;
//
//    if (ch == '(')
//    {
//        return 3;
//    }
//    if (ch == ')')
//    {
//        return 0;
//    }
//}
//
//void TransToEnd(string s, stack<char>& data)
//{
//    stack<char> sk;//记录运算符
//    int flag = 0;//标记括号
//    for (auto c : s)
//    {
//        if (c == ' ')
//            continue;
//        if (isdigit(c))
//        {
//            data.push(c);
//        }
//        else if (c == '(')
//        {
//            sk.push(c);
//            flag = 1;
//        }
//        else if (c == ')')
//        {
//            while (sk.top() != '(')
//            {
//                data.push(sk.top());
//                sk.pop();
//            }
//            sk.pop();//去掉(;
//        }
//        else if (sk.empty()
//            || GetPriority(c) > GetPriority(sk.top())
//            || flag == 1
//            )
//        {
//            sk.push(c);
//            flag = 0;
//        }
//        else if (GetPriority(c) <= GetPriority(sk.top()))
//        {
//            while (!sk.empty() && GetPriority(c) <= GetPriority(sk.top()))
//            {
//                data.push(sk.top());
//                sk.pop();
//            }
//            sk.push(c);
//        }
//    }
//
//    while (!sk.empty())
//    {
//        data.push(sk.top());
//        sk.pop();
//    }
//}
//int main()
//{
//    string s;
//    stack<char> data;
//    TransToEnd("(10+20/2*3)/2+8", data);
//    while (!data.empty())
//    {
//        s += data.top();
//        data.pop();
//    }
//    reverse(s.begin(), s.end());
//    cout << s << endl;
//    return 0;
//}

class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     * 返回表达式的值
     * @param s string字符串 待计算的表达式
     * @return int整型
     */
    int solve(string s)
    {
        // write code here
        string data;
        stack<char> num;
        TransToEnd(s, data);
        cout << data << endl;
        return 0;
    }


    inline int GetPriority(char ch)
    {
        if (ch == '+' || ch == '-')
            return 1;
        if (ch == '*' || ch == '/')
            return 2;

        if (ch == '(')
        {
            return 3;
        }
        if (ch == ')')
        {
            return 0;
        }
    }

    void TransToEnd(string s, string& data)
    {
        stack<char> sk;//记录运算符
        int flag = 0;//标记括号
        for (auto c : s)
        {
            if (c == ' ')
                continue;
            if (isdigit(c))
            {
                data += c;
            }
            else if (c == '(')
            {
                sk.push(c);
                flag = 1;
            }
            else if (c == ')')
            {
                while (sk.top() != '(')
                {
                    data += sk.top();
                    sk.pop();
                }
                sk.pop();//去掉(;
            }
            else if (sk.empty()
                || GetPriority(c) > GetPriority(sk.top())
                || flag == 1
                )
            {
                sk.push(c);
                flag = 0;
            }
            else if (GetPriority(c) <= GetPriority(sk.top()))
            {
                while (!sk.empty() && GetPriority(c) <= GetPriority(sk.top()))
                {
                    data += sk.top();
                    sk.pop();
                }
                sk.push(c);
            }
        }

        while (!sk.empty())
        {
            data += sk.top();
            sk.pop();
        }
    }
};


int main()
{
    cout << Solution().solve("(1 + 3 / 2 + 2) * 3") << endl;
}