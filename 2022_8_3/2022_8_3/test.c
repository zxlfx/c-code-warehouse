#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cstring>
using namespace std;

//template<class T, size_t N>
//class Array
//{
//public:
//	void f()
//	{
//		N = 3;
//	}
//private:
//	T a[N];
//};
//
//int main()
//{
//	Array<int, 10> a;
//	a.f();
//	return 0;
//}

// 函数模板 -- 参数匹配
//template<class T>
//bool Less(T left, T right) 
//{
//	return left < right;
//}
//
//
//bool Less(const char* left, const char* right)
//{
//	return strcmp(left, right) < 0;
//}
//
//int main()
//{
//	cout << Less(1, 2) << endl; // 可以比较，结果正确
//	cout << Less("a", "d") << endl;;// 可以比较，结果错误
//	return 0;
//}

template<class T1, class T2>
class Data
{
public:
	Data() { cout << "Data<T1, T2>" << endl; }
private:
	T1 _d1;
	T2 _d2;
};

template<>//特化版本
class Data<int, char> 
{
public:
	Data() { cout << "Data<int, char>" << endl; }
private:
	int _d1;
	char _d2;
};

// 将第二个参数特化为int
template <class T1>
class Data<T1, int> 
{
public:
	Data() { cout << "Data<T1, int>" << endl; }
private:
	T1 _d1;
	int _d2;
};

//两个参数偏特化为指针类型
template <typename T1, typename T2>
class Data <T1*, T2*>
{
public:
	Data() 
	{ 
		cout << "Data<T1*, T2*>" << endl; 
	}

private:
	T1 _d1;
	T2 _d2;
};