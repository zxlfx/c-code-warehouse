#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//class A 
//{
//public:
//	void test(float a) { cout << "1"; }
//};
//class B :public A 
//{
//public:
//	void test(int b) { cout << "2"; }
//};


//class A 
//{
//protected:
//	void func()
//	{
//		cout << "A::func()" << endl;
//	}
//};
//struct B :A 
//{
//	void virtual print() = 0;
//};


//#include <iostream>
//using namespace std;
//class A {
//public:
//	~A() 
//	{
//		cout << "~A()";
//	}
//};
//class B {
//public:
//	virtual ~B() 
//	{
//		cout << "~B()";
//	}
//}; 
//
//class C : public A, public B
//{
//public:
//	~C() 
//	{
//		cout << "~C()";
//	}
//};
//
//int main() {
//	C* c = new C;
//	B* b1 = dynamic_cast<B*>(c);
//	A* a2 = dynamic_cast<A*>(b1);
//	delete a2;
//}