#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

class Solution {
public:
    vector<vector<int>> generate(int numRows)
    {
        vector<vector<int>> vv;
        vv.resize(numRows);

        for (int i = 0; i < vv.size(); i++)
        {
            vv[i].resize(i + 1, 1);
        }

        for (int i = 0; i < vv.size(); i++)
        {
            for (int j = 0; j < vv[i].size(); j++)
            {
                if (j == 0 || j == vv[i].size() - 1)
                {
                    continue;
                }
                else
                {
                    vv[i][j] = vv[i - 1][j-1] + vv[i - 1][j];
                }
            }
        }

        return vv;
    }
};

int main()
{
    Solution().generate(5);
	return 0;
}