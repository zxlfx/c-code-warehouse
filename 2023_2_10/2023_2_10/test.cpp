#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<windows.h>
using namespace std;

//namespace RAII
//{
//	template<class T>
//	class SmartPtr
//	{
//	public:
//		SmartPtr(T* ptr)
//			:_ptr(ptr)
//		{}
//
//		~SmartPtr()
//		{
//			delete[] _ptr;
//		}
//
//		T& operator*()
//		{
//			return *_ptr;
//		}
//
//		T* operator->()
//		{
//			return _ptr;
//		}
//
//	private:
//		T* _ptr;
//	};
//
//	template<class T>
//	class auto_ptr
//	{
//	public:
//		auto_ptr(T* ptr)
//			:_ptr(ptr)
//		{}
//
//		auto_ptr(const auto_ptr& ap)
//			:_ptr(ap._ptr)
//		{
//			ap._ptr = nullptr;
//		}
//
//		~auto_ptr()
//		{
//			delete[] _ptr;
//		}
//
//		T& operator*()
//		{
//			return *_ptr;
//		}
//
//		T* operator->()
//		{
//			return _ptr;
//		}
//
//	private:
//		T* _ptr;
//	};
//}

//void test1()//auto_ptr测试
//{
//	//管理权转移
//	auto_ptr<int> ap1(new int[10]);
//	cout << ap1.get() << endl;
//	auto_ptr<int> ap2(ap1);
//
//	//ap1被悬空
//	cout << ap1.get() << endl;
//	cout << ap2.get() << endl;
//
//	//很多公司明确不能使用auto_ptr
//}

//void test2()
//{
//	const int a = 10;
//	int* b = reinterpret_cast<int*>(&a);
//	*b = 20;
//	cout << "a = " << a << endl;
//	cout << "b = " << *b << endl;
//}

//class A
//{
//public:
//	virtual void f()
//	{
//		cout << "A()" << endl;
//	}
//};
//
//class B:public A
//{
//public:
//	virtual void f()
//	{
//		cout << "B()" << endl;
//	}
//
//	int _b = 10;
//};

//void test3(A& a)
//{
//	////a.f();
//	//B& b = reinterpret_cast<B&>(a);
//	//cout << &b;
//	//cout << b._b << endl;
//}

//int main()
//{
//	//A a;
//	//B b;
//	////cout << &b << endl;
//	//cout << &a << endl;
//	//test3(a);
//	//A a;
//	//B b = reinterpret_cast<B>(a);
//	//test1();
//	//A* a = new A();
//	//B* b = a;
//	/*cout << a << endl;
//	B* a1 = reinterpret_cast<B*>(a);
//	cout << a1 << endl;
//
//	B* b = new B();
//	a = b;
//	a->f();
//	cout << b << endl;
//
//	B* c = dynamic_cast<B*>(a);
//	cout << c << endl;*/
//	return 0;
//}


class A
{
public:
	~A()
	{
		cout << "~A()" << endl;
	}
	A()
	{
		cout << "A()" << endl;
	}
	static A a;
};
A A::a;


int main()
{
	int n = 10;
	while (n--)
	{
		cout << "mian()" << endl;
	}
	return 0;
}