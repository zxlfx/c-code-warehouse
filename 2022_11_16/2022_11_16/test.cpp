#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cstdio>
using namespace std;
typedef long long LL;
LL n;//注意用long long
int main() {
    freopen("chicken.in", "r", stdin);
    freopen("chicken.out", "w", stdout);
    cin >> n;
    LL l = n / 4 + (n % 4 != 0);//k的下限
    LL r = n * 2 / 7;//k的上限
    if (r < l) {
        puts("No Answer.");
    }
    else {
        cout << r - l + 1 << endl;//合法的k的数量
    }
    return 0;
}