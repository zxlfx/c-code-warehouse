#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<queue>
#include<vector>
using namespace std;

class Solution {
public:
    vector<int> smallestK(vector<int>& arr, int k) {

        priority_queue<int> p;
        vector<int> ret;
        //满足k==0时的测试用例
        if (k == 0)
        {
            return ret;
        }

        //用前k个数建立大堆
        for (int i = 0; i < k; i++)
        {
            p.push(arr[i]);
        }

        //p.top()位置是大堆最大的数，依次比较剩余arr的数，如果arr[i] < p.top()，那么就将该数与p.top()进行“替换”
        for (int i = k; i < arr.size(); i++)
        {
            if (arr[i] < p.top())
            {
                p.pop();
                p.push(arr[i]);
            }
        }

        //经过上面的遍历替换，大堆中的k个数即为arr数组中前k个最小的数
        while (!p.empty())
        {
            ret.push_back(p.top());
            p.pop();
        }
        return ret;
    }
};

int main()
{
    vector<int> v = { 1,-1,5,6,0 };
    vector<int> ret = Solution().smallestK(v, 2);
    for (auto& e : ret)
    {
        cout << e << " ";
    }
    cout << endl;
    return 0;
}