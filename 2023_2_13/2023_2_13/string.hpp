#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string.h>
#include<assert.h>
using namespace std;

namespace ds
{
	class string
	{
	public:
		typedef char* iterator;
		typedef const char* const_iterator;

		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		const_iterator begin()const 
		{
			return _str;
		}

		const_iterator end()const
		{
			return _str + _size;
		}

		void reserve(size_t capacity)
		{
			if (capacity > _capacity)
			{
				char* temp = new char[capacity + 1];
				strcpy(temp, _str);
				delete[] _str;
				_str = temp;
				_capacity = capacity;
			}
		}

		void insert(size_t pos, const char* str)
		{
			assert(pos <= size());
			int insize = strlen(str);
			if (size() + insize > capacity())
			{
				reserve(size() + insize);
			}

			char* end = _str + _size;
			char* start = _str + pos;
			while (end >= start)
			{
				*(end + insize) = *end;
				end--;
			}

			strncpy(_str + pos, str, insize);
			_size += insize;
		}

		size_t size()const
		{
			return _size;
		}

		size_t capacity()const
		{
			return _capacity;
		}

		void push_back(int ch)
		{
			insert(_size, ch);
		}

		void append(const char* str)
		{
			insert(size(), str);
		}

		void resize(size_t n , char ch)
		{
			if (n > size())
			{
				for (int i = size(); i < n; i++)
				{
					push_back(ch);
				}
			}
		}

		void insert(size_t pos,char c)
		{
			assert(pos <= _size);
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}
			char* end = _str + _size;
			char* start = _str + pos;
			while (end >= start)
			{
				*(end + 1) = *end;
				end--;
			}
			_str[pos] = c;
			_size++;
		}

		string(const char* str = "")
			:_size(strlen(str))
			,_capacity(_size)
			,_str(new char[_capacity + 1])
		{
			strcpy(_str, str);
		}

		~string()
		{
			if (_str)
			{
				delete[] _str;
				_size = _capacity = 0;
			}
		}

		char operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}
	private:
		int _size;
		int _capacity;
		char* _str;
	};

	ostream& operator<<(ostream& out, string& s)
	{
		for (int i = 0; i < s.size(); i++)
		{
			out << s[i];
		}
		return out;
	}
}