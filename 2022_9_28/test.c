#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//class B;
//class A
//{
//	 friend void B::Bfun();
//public:
//	int a;  
//private:
//	void Afun();
//};
//
//
//class B
//{
//public:
//	A* a;
//	int b;
//	void Bfun();
//};
//
//
//void B::Bfun()
//{
//	a->Afun();
//}
//
//void A::Afun()
//{
//	cout << "A的私有函数\n";
//}
//
//int main()
//{
//	B b;
//	b.Bfun();
//	return 0;
//}


class A;
class B
{
public:
	A* a;
	int b;
	void Bfun();
};

class A
{
	friend void B::Bfun();
public:
	int a;
private:
	void Afun();
};





void B::Bfun()
{
	a->Afun();
}

void A::Afun()
{
	cout << "A的私有函数\n";
}

int main()
{
	B b;
	b.Bfun();
	return 0;
}