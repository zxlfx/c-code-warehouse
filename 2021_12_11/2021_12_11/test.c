private:
	kedaadd  adds;                         //其他功能操作的类，每个按钮对应一个子窗口         
	kedachange changes;      //前缀keda是我的名字，后面的英文都很直接地解释相对应的功能
	kedadelete deletes;
	kedasave saves;
	kedaimport imports;
	kedafinds finds;
	Student one_student;
public:
	UINT m_line;                 //计算列表框的行数
	UINT m_index;                //用于计算循环的变量
	UINT m_indexmax;             //列数，就是8
	BOOL judge;
	BOOL check;
	CString filename;
	UINT m_stusum;                 //计算学生总数
	UINT m_Row;
	UINT m_Col;

	BOOL nodata();               //添加函数
	BOOL empty();
	BOOL checkgrade();
	BOOL checknumber(char(&c)[15]);
	BOOL checkxingbie();

	void deletenull();           //删除函数
	void delete_q();

	void find_q();               //查找函数
	void reset(UINT a);

	void importtt();             //导入函数
	UINT importt();

	void findcheck();            //修改函数
	void settext(int line);
	BOOL checknum();
	void changename();

	void resort(UINT x);               //排序函数


