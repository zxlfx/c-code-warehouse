#pragma once
#pragma warning(disable:4996)

#include<string>
#include<iostream>
#include<assert.h>
#include<string.h>
using namespace std;

namespace KN1
{
	//模拟实现一个string类
	class string
	{
	public:
		//默认构造函数
		string(const char* str = "")
			:_size(strlen(str))
			, _capacity(_size)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}

		void swap(string& s)//std中的swap与string中的swap是不一样的
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}

		//拷贝构造现代写法(推荐使用)  s1(s)
		string(const string& s) //利用现成的构造函数帮忙
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			string tmp(s._str);//这里的tmp生命周期在函数内,所以函数调用结束时会直接调用析构函数将其销毁
			swap(tmp);
		}

		//赋值重载现代写法 (s2=s)
		string operator=(string s)//传值传参进行一次拷贝构造,s能够在出函数作用域时调用析构函数进行销毁
		{
			swap(s);
			return *this;//由于s的生命周期,所以不能传引用返回！
		}

		//析构函数
		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = _capacity = 0;
		}

		//实现size()
		size_t size() const
		{
			return _size;
		}

		//实现c_str
		const char* c_str() const
		{
			return _str;
		}

		//实现reserve()
		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity = n + 1;
			}
		}

		//实现find()
		size_t find(char ch) const//要清楚是在对象的_str中找某个字符
		{
			int i = 0;
			for (i = 0; i < _size; i++)
			{
				if (_str[i] == ch)
				{
					return i;
				}
			}
			return npos;
		}
		size_t find(const char* str, size_t pos = 0) const
		{
			//最好可以自己实现一个查找字串的的算法
			const char* ptr = strstr(_str + pos, str);
			if (ptr == nullptr)
			{
				return npos;
			}
			return (ptr - _str);//指针减指针
		}

		//实现insert()
		string& insert(size_t pos, char ch)
		{
			assert(pos <= _size);
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}
			//这里循环的判断条件不可以有i=pos,因为pos=0进行头插时会越界;
			//并且可以通过初始化i=_size+1使'\0'也进行移动
			for (size_t i = _size + 1; i > pos; i--)
			{
				_str[i] = _str[i - 1];//数据挪动,要控制下标
			}
			_str[pos] = ch;
			_size++;
			return *this;
		}

		string& insert(size_t pos, const char* str)
		{
			assert(pos <= _size);
			size_t len = strlen(str);
			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}
			for (size_t i = _size + len; i >= pos + len; i--)
			{
				_str[i] = _str[i - len];
			}
			//strcpy(_str + pos, str); 这个会把'\0'也拷过去
			strncpy(_str + pos, str, len);
			_size += len;
			return *this;
		}

	private:
		char* _str;//指向字符串首元素的指针
		size_t _size;
		size_t _capacity;
		static const size_t npos;
	};
	const size_t string::npos = -1;

	void test4()
	{
		string s("hello world");
		string s1("orl");
		cout << s.c_str() << endl;
		size_t pos2 = s.find(s1.c_str());
		s.insert(pos2, "1234567");
		size_t pos1 = s.find('l');
		s.insert(pos1, 'x');
		cout << s.c_str() << endl;   //最后对s._str析构是会进行报错
	}
}
