#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<assert.h>
using namespace std;

//struct ListNode
//{
//	ListNode(int val = 0)
//	{
//		_val = val;
//		_next = nullptr;
//	}
//private:
//	int _val;
//	ListNode* _next;
//};
//int main()
//{
//	ListNode* n = new ListNode;
//	const ListNode& n1 = ListNode();
//	return 0;
//}

//int& func1()
//{
//	int* a = new int(10);
//	return *a;
//}
//
//int* func2()
//{
//	int* a = new int(10);
//	return a;
//}
//int main()
//{
//	int a = func1();
//	int b = *(func2());
//	return 0;
//}


namespace ds 
{
	class Date
	{
	private:
		int GetMonthDay(int year, int month)
		{
			static int days[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
			if ((month == 2) && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
			{
				return 29;
			}
			return days[month];
		}
	public:
		friend ostream& operator<<(const Date& d, ostream& out);
		friend istream& operator>>(Date& d, istream& in);
		Date operator+(int day);
		Date& operator+=(int day);
		Date operator-(int day);
		Date& operator-=(int day);
		Date operator-(const Date& d)const;
		Date& operator++();
		Date operator++(int);
		Date& operator--();
		Date operator--(int);
		Date& operator=(const Date& d);

		bool operator==(const Date& d)const;
		bool operator!=(const Date& d)const;
		bool operator>(const Date& d)const;
		bool operator>=(const Date& d)const;
		bool operator<(const Date& d)const;
		bool operator<=(const Date& d)const;



		Date(int year = 2022, int month = 11, int day = 7)
			:_year(year),
			_month(month),
			_day(day)
		{
			assert(_year > 0 
				&& _month >= 1 && _month <= 12 
				&& _day >= 1 && _day <= GetMonthDay(_year, _month));
		}

		Date(const Date& d)
		{
			_year = d._year;
			_month = d._month;
			_day = d._day;
		}

		Date& operator+=(int day)
		{
			_day += day;
			while (_day > GetMonthDay(_year, _month))
			{
				++_month;
				_day -= GetMonthDay(_year, _month);
				if (_month > 12)
				{
					_month = 1;
					_year++;
				}
			}
		}

		Date operator-(const Date& d)const
		{
			int flag = 0;
			Date max = *this;
			Date min = d;
			int cnt = 0;
			if (max < min)
			{
				swap(max, min);
				flag = -1;
			}
			while (max != min)
			{
				min++;
				cnt++;
			}
			return cnt * flag;
		}

		Date operator+(int day)
		{
			Date temp = *this;
			temp -= day;
			return temp;
		}

		Date& operator-=(int day)
		{
			_day -= day;
			if (_day < 1)
			{
				_month--;
				_day += GetMonthDay(_year, _month);
				if (_month == 0)
				{
					_year = 1;
					_month = 12;
				}
			}
		}

		Date operator-(int day)
		{
			Date temp = *this;
			temp -= day;
			return temp;
		}

		Date& operator=(const Date& d)
		{
			if (this != &d)
			{
				_year = d._year;
				_month = d._month;
				_day = d._day;
			}
		}

		Date& operator++()
		{
			*this += 1;
			return *this;
		}

		Date operator++(int)
		{
			Date temp = *this;
			*this += 1;
			return temp;
		}


		Date& operator--()
		{
			*this -= 1;
			return *this;
		}

		Date operator--(int)
		{
			Date temp = *this;
			*this -= 1;
			return temp;
		}

		bool operator>(const Date& d)const
		{
			if ((_year > d._year)
				|| (_year == d._year && _month > d._month)
				|| (_year == d._year && _month == d._month && _day > d._day)
				)
			{
				return true;
			}
			else
			{
				false;
			}
		}

		bool operator==(const Date& d)const
		{
			return _year == d._year && _month == d._month && _day == d._day;
		}

		bool operator!=(const Date& d)const
		{
			return !(*this == d);
		}

		bool operator>=(const Date& d)const
		{
			return (*this > d) || (*this == d);
		}

		bool operator<(const Date& d)const
		{
			return d > *this;
		}

		bool operator<=(const Date& d)const
		{
			!(*this > d);
		}

	private:
		int _year;
		int _month;
		int _day;
	};

	ostream& operator<<(const Date& d, ostream& out)
	{
		out << d._year << "��" << d._month << "��" << d._day << "��" << endl;
		return out;
	}

	istream& operator>>(Date& d, istream& in)
	{
		in >> d._year >> d._month >> d._day;
		return in;
	}

}
int main()
{
	
	return 0;
}