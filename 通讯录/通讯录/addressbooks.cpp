#include<iostream>
#include<string>
#define MAX 100
using namespace std;
void menu()
{
	cout << "************************" << endl;
	cout << "***** 1.添加联系人 *****" << endl;
	cout << "***** 2.显示联系人 *****" << endl;
	cout << "***** 3.查找联系人 *****" << endl;
	cout << "***** 4.删除联系人 *****" << endl;
	cout << "***** 5.修改联系人 *****" << endl;
	cout << "***** 6.清空通讯录 *****" << endl;
	cout << "***** 0.退出通讯录 *****" << endl;
	cout << "************************" << endl;
}

struct people
{
	string m_name;
	int m_age;
	int m_sex;
	string m_phone;
	string m_address;
};

struct addressbooks
{
	struct people people_arr[MAX];
	int m_size;
};

void add_people(struct addressbooks* p)
{
	if (p->m_size != MAX)
	{
		string name;
		cout << "请输入姓名:   " << endl;
		cin >> name;
		p->people_arr[p->m_size].m_name = name;

		int age = 0;
		cout << "请输入年龄:  " << endl;
		cin >> age;
		p->people_arr[p->m_size].m_age = age;

		int sex = 0;
		while (true)
		{
			cout << "请输入性别:  " << endl;
			cout << "1.男" << endl;
			cout << "2.女" << endl;
			cin >> sex;
			if (sex == 1 || sex == 2)
			{
				p->people_arr[p->m_size].m_sex = sex;
				break;
			}
			else
			{
				cout << "输入不合法，请重新输入" << endl;
			}
		}

		string phone;
		cout << "请输入电话:  " << endl;
		cin >> phone;
		p->people_arr[p->m_size].m_phone = phone;

		string address;
		cout << "请输入地址:  " << endl;
		cin >> address;
		p->people_arr[p->m_size].m_address = address;
		cout << "添加成功" << endl;
		p->m_size++;
	}
	else
	{
		cout << "通讯录已满  " << endl;
	}
	system("pause");
	system("cls");
}

void show_people(struct addressbooks* p)
{
	if (p->m_size == 0)
	{
		cout << "当前通讯录为空" << endl;
	}
	else
	{
		int i = 0;
		for (i = 0; i < p->m_size; i++)
		{
			cout << "姓名:  "  << p->people_arr[i].m_name << "\t"
				 << "年龄:  "  << p->people_arr[i].m_age << "\t"
				 << "性别:  "  << (p->people_arr[i].m_sex==1?"男":"女") << "\t"
				 << "电话:  "  << p->people_arr[i].m_phone << "\t"
				 << "住址:  "  << p->people_arr[i].m_address << endl;
		}
	}
	system("pause");
	system("cls");
}

static int is_exit(struct addressbooks* p,string name)
{
	int i = 0;
	for (i = 0; i < p->m_size; i++)
	{
		if (p->people_arr[i].m_name == name)
		{
			return i;
		}
	}
	return -1;

}

void find_people(struct addressbooks* p)
{
	string name;
	cout << "请输入要查找的姓名:  " << endl;
	cin >> name;
	int ret = is_exit(p, name);
	if (ret == -1)
	{
		cout << "查无此人" << endl;
	}
	else
	{
		cout << "姓名: "  << p->people_arr[ret].m_name << "\t"
			 << "年龄:  " << p->people_arr[ret].m_age << "\t"
			 << "性别:  " << (p->people_arr[ret].m_sex==1?"男":"女") << "\t"
			 << "电话:  " << p->people_arr[ret].m_phone << "\t"
			 << "住址： " << p->people_arr[ret].m_address << endl;
	}
	system("pause");
	system("cls");
}

void delete_people(struct addressbooks* p)
{
	string name;
	cout << "请输入要删除的姓名:  " << endl;
	cin >> name;
	int ret = is_exit(p, name);
	if (ret == -1)
	{
		cout << "要删除的姓名不存在" << endl;
	}
	else
	{
		int i = 0;
		p->m_size--;
		for (i = ret; i < (p->m_size - 1); i++)
		{
			p->people_arr[i] = p->people_arr[i + 1];
		}
		cout << "删除完成" << endl;
	}
	system("pause");
	system("cls");
}

void modify_people(struct addressbooks* p)
{
	string name;
	cout << "请输入要修改的姓名:  " << endl;
	cin >> name;
	int ret = is_exit(p, name);
	if (ret == -1)
	{
		cout << "要修改的人不在通讯录中" << endl;
	}
	else
	{
		cout << "请输入姓名:  " << endl;
		cin >> p->people_arr[ret].m_name;
		cout << "请输入年龄： " << endl; 
		cin >> p->people_arr[ret].m_age;
		cout << "请输入性别:  " << endl <<"1.男" << endl << "2.女" << endl;
		cin >> p->people_arr[ret].m_sex;
		cout << "请输入电话:  " << endl;
		cin >> p->people_arr[ret].m_phone;
		cout << "请输入地址:  " << endl;
		cin >> p->people_arr[ret].m_address;
	}
	system("pause");
	system("cls");
}

void empty_people(struct addressbooks* p)
{
	if (p->m_size > 0)
	{
		p->m_size = 0;
		cout << "已清空通讯录" << endl;
	}
	else
	{
		cout << "通讯录已为空，不需要再清空" << endl;
	}
	system("pause");
	system("cls");
}

int main()
{
	int select = 0;
	struct addressbooks ads;
	ads.m_size = 0;
	do
	{
		menu();
		cout << "请选择:>" << endl;
		cin >> select;
		switch (select)
		{
		case 1://添加联系人
			add_people(&ads);
			break;
		case 2://显示联系人
			show_people(&ads);
			break;
		case 3://查找联系人
			find_people(&ads);
			break;
		case 4://删除联系人
			delete_people(&ads);
			break;
		case 5://修改联系人
			modify_people(&ads);
			break;
		case 6://清空联系人
			empty_people(&ads);
			break;
		case 0:
			cout << "已退出通讯录" << endl;
			break;
		default:
			cout << "输入错误，请重新输入" << endl;
			system("pause");
			system("cls");
			break;
		}
	} while (select);
	system("pause");
	return 0;
}