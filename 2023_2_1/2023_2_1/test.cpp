#define _CRT_SECURE_NO_WARNINGS 1
#include"my_prority_queue.h"

void test1()
{
	ds::priority_queue<int,vector<int>,greater<int>> pq;
	pq.push(1);
	pq.push(5);
	pq.push(2);
	pq.push(4);
	pq.push(8);
	pq.push(7);

	while (!pq.empty())
	{
		cout << pq.top() << " ";
		pq.pop();
	}
	cout << endl;
}
int main()
{
	test1();
	return 0;
}