#pragma once
#include<iostream>
#include<vector>
#include<assert.h>
//#include<functional>

using namespace std;

namespace ds
{
	template<class T,class Container = vector<T>,class Compare = less<T>>
	class priority_queue
	{
	private:
		void AdjustUp(int child)
		{
			Compare cmp;
			int partent = (child - 1) / 2;
			while (child > 0)
			{
				if (cmp(_con[partent] , _con[child]))
				{
					std::swap(_con[partent], _con[child]);
					child = partent;
					partent = (child - 1) / 2;
				}
				else
				{
					break;
				}
			}
		}

		void AdjustDown(int partent)
		{
			Compare cmp;
			int child = partent * 2 + 1;
			while (child < size())
			{
				if (child + 1 < size() && cmp(_con[child] , _con[child + 1]))
				{
					child++;
				}
				if (cmp(_con[partent] , _con[child]))
				{
					std::swap(_con[partent], _con[child]);
					partent = child;
					child = partent * 2 + 1;
				}
				else
				{
					break;
				}
			}
		}

	public:
		void push(const T& x)
		{
			_con.push_back(x);
			AdjustUp(size() - 1);
		}

		void pop()
		{
			assert(!empty());
			swap(_con[0], _con[size() - 1]);
			_con.pop_back();
			AdjustDown(0);
		}

		size_t size()
		{
			return _con.size();
		}

		bool empty()
		{
			return _con.empty();
		}

		const T& top()
		{
			return _con[0];
		}
	private:
		Container _con;
	};
}
